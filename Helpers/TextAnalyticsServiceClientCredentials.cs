﻿using System.Configuration;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Rest;

namespace SimpleEchoBot.Helpers
{
    public class TextAnalyticsServiceClientCredentials : ServiceClientCredentials
    {
        public override Task ProcessHttpRequestAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            request.Headers.Add("Ocp-Apim-Subscription-Key", GetApiKey());
            return base.ProcessHttpRequestAsync(request, cancellationToken);
        }

        public string GetApiKey()
        {
            return ConfigurationManager.AppSettings["TextAnalyticsApiKey"];
        }

        public string GetBaseUri()
        {
            string uri = ConfigurationManager.AppSettings.Get("TextAnalyticsUri");
            return uri != null ? uri : "https://westeurope.api.cognitive.microsoft.com/text/analytics/v2.0";
        }
    }
}