﻿using System.Xml.Serialization;

namespace SimpleEchoBot.Helpers
{
    public class AwardReasonTable
    {
        [XmlElement("PKID")]
        public string pkid;
        [XmlElement("Language")]
        public string language;
        [XmlElement("AwardReasonName")]
        public string awardReasonName;
        [XmlElement("EnumVal")]
        public string enumVal;
    }
}
