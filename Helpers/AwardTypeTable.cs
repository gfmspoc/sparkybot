﻿using System.Xml.Serialization;


namespace SimpleEchoBot.Helpers
{
    public class AwardTypeTable
    {

        [XmlElement("PK_AWARD_TYPE")]
        public string pkAwardType;
        [XmlElement("Value")]
        public string value;
        [XmlElement("EnumValue")]
        public string enumValue;

    }
}
