﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;


namespace SimpleEchoBot.Helpers
{
    [Serializable]
    [XmlRoot("GiftCardList")]
    public class GiftCardList
    {
        public GiftCardList() { Items = new List<GiftCard>(); }
        [XmlElement("GiftCard")]
        public List<GiftCard> Items { get; set; }
    }

    [Serializable]
    public class GiftCard
    {

        [XmlElement("PKID")]
        public string PKID { get; set; }

        [XmlElement("Description")]
        public string description { get; set; }

        [XmlElement("Denomination")]
        public string denomination { get; set; }

        [XmlElement("EnumVal")]
        public string enumVal { get; set; }


        public override string ToString()
        {
            return $"{description}";
        }
    }
}
