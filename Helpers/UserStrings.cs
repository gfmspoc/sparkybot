﻿namespace SimpleEchoBot.Helpers
{
    public static class UserStrings
    {
        //setup nomination process strings

        public const string NominationStepSelectRecepients = "Hi {0}, I am Sparky, your virtual assistant and I’m here to help you through your first nomination. \n\n" +
                                                      "Many of the colleagues you work with have given recognition on Globostars. To get started, please search for or select a colleague you would like to nominate. You can choose one, many or perhaps even choose a team to nominate. \n\n" +
                                                     "Feel free to ask me any questions throughout!";

        public const string NominationStepNominationSelectPrograms = "Great, I see you’re nominating {0}. \n\n" +
                                                                     "Here, you must select the recognition program in which you want to give recognition.";

        public const string NominationStepSelectReason = "Great, I see you’re nominating {0}. \n\n" +
                                                                     "Here you will choose an award reason from the list. Typically award reasons exemplify our companies’ values. \n\n" +
                                                                     "Let’s select an award reason which you think best matches how James has helped you.";

        public const string NominationStepSelectAward = "Great, you can choose a value for your award. Don’t worry about choosing an unsuitable amount as this will be approved by {0}'s manager first.\n\n" +
                                                        "If it helps, many of your colleagues have given **50 EUR** when giving their first award.";

        public const string NominationStepNominationMessage = "Excellent, now for the fun part! Don’t hold back on the good vibes. \n\n" +
                                                            "Here you get the opportunity to write a positive message to {0}, thanking or showing your appreciation for a job well done. \n\n" +
                                                            "If you like, I can offer some feedback on the message you have written before you send it?";

        public const string NominationStepCreatedNotification = "Congratulations {0}, in a simple few steps you made a recognition moment " +
                                                                "that will have a lasting and positive impact on your colleague, {1}. \n\n This is an important part" +
                                                                " of our organization’s culture, thank you!";
    }
}
