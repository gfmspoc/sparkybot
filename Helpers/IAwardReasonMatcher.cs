﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SimpleEchoBot.Models;

namespace SimpleEchoBot.Helpers
{
    /**
     * A matcher takes a set of generic classification labels and maps these to
     * a set of award reasons that are specific to the company relevant to the
     * context.
     */
    public interface IAwardReasonMatcher
    {
        Task<IList<AwardReason>> MatchAwardReasons(IList<AwardReason> reasons, Dictionary<string, object> context);
    }
}