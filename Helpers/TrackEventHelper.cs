﻿using System.Collections.Generic;
using Microsoft.ApplicationInsights;
using Microsoft.Bot.Connector;

namespace SimpleEchoBot.Helpers
{
    public class TrackEventHelper
    {
        public static void TrackEvent(string eventName, string step, IActivity activity)
        {
            TelemetryClient telemetryClient = new TelemetryClient();
            telemetryClient.Context.User.Id = activity.From.Name;
            telemetryClient.Context.Operation.Name = step;
            telemetryClient.Context.Operation.ParentId = activity.Conversation.Id;

            var properties = new Dictionary<string, string> {{"Step", step}, { "ChannelId", activity.ChannelId } };

            telemetryClient.TrackEvent(eventName, properties);
        }
    }
}