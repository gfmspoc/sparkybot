﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SimpleEchoBot.Models;

namespace SimpleEchoBot.Helpers
{
    public interface IAwardReasonClassifier
    {
        Task<IList<AwardReason>> ClassifyAwardReasons(string userText, Dictionary<string, object> context);
    }
}