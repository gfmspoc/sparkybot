﻿using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using Microsoft.Bot.Builder.Luis.Models;

namespace SimpleEchoBot.Helpers
{
    public class LuisResponse
    {
        public string query { get; set; }
        public TopScoringIntent topScoringIntent { get; set; }
        public List<Intent> intents { get; set; }
        public List<object> entities { get; set; }
    }

    public class TopScoringIntent
    {
        public string intent { get; set; }
        public double score { get; set; }
    }

    public class Intent
    {
        public string intent { get; set; }
        public double score { get; set; }
    }

    public class QnaResponse
    {
        public List<Answer> answers { get; set; }
    }

    public class Answer
    {
        public List<string> questions { get; set; }
        public string answer { get; set; }
        public double score { get; set; }
        public int id { get; set; }
        public string source { get; set; }
        public List<object> metadata { get; set; }
    }

    public class HttpClientHelper
    {
        public static async Task<string> GetAsync(string uri, Dictionary<string, string> headers)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );

                foreach (var header in headers)
                {
                    client.DefaultRequestHeaders.Add(header.Key, header.Value);
                }

                var response = await client.GetAsync(uri);

                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsStringAsync();
            }
        }

        public static async Task<string> PostAsync(string uri, Dictionary<string, string> headers,
            StringContent content)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );

                foreach (var header in headers)
                {
                    client.DefaultRequestHeaders.Add(header.Key, header.Value);
                }

                var response = await client.PostAsync(uri, content);

                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsStringAsync();
            }
        }

        public static async Task<string> MakeTranslatorRequest(string userInput, string from, string to)
        {
            var host = "https://api.cognitive.microsofttranslator.com";
            var path = "/translate?api-version=3.0";
            var key = ConfigurationManager.AppSettings["TranslatorKey"];
            var param = string.Format("&to={0}", to);

            var uri = host + path + param;

            var jsonBody =
                new StringContent("[{ \"text\" : \"" + userInput + "\"}]", Encoding.UTF8, "application/json");

            var headers = new Dictionary<string, string>
            {
                {"Ocp-Apim-Subscription-Key", key}
            };

            var responseContent = await PostAsync(uri, headers, jsonBody);
            return responseContent;
        }


        public static async Task<LuisResult> MakeLUISRequest(string userInput)
        {
            
            var luisAppId = ConfigurationManager.AppSettings["LUISAppId"];
            var luisAPIKey = ConfigurationManager.AppSettings["LUISAPIKey"];
            var luisAPIHostName = ConfigurationManager.AppSettings["LUISAPIHostName"];

            var queryString = HttpUtility.ParseQueryString(string.Empty);

            queryString["q"] = userInput;
            queryString["timezoneOffset"] = "0";
            queryString["verbose"] = "true";
            queryString["spellCheck"] = "false";
            queryString["staging"] = "false";

            var uri = $"https://{luisAPIHostName}/luis/v2.0/apps/{luisAppId}?{queryString}";

            var headers = new Dictionary<string, string>
            {
                {"Ocp-Apim-Subscription-Key", luisAPIKey}
            };

            return JsonConvert.DeserializeObject<LuisResult>(await GetAsync(uri, headers));
        }


        public static async Task<QnaResponse> MakeQNARequest(string userInput)
        {
            var queryString = HttpUtility.ParseQueryString(string.Empty);

            var qnaAPIKey = ConfigurationManager.AppSettings["QNAAPIKey"];
            var qnaKbId = ConfigurationManager.AppSettings["QNAKbId"];
            var qnaHostName = ConfigurationManager.AppSettings["QNAHostName"];

            var headers = new Dictionary<string, string>
            {
                {"Authorization", "EndpointKey " + qnaAPIKey}
            };

            var uri = $"https://{qnaHostName}/qnamaker/knowledgebases/{qnaKbId}/generateAnswer";
            var body = new StringContent("{\"question\": \"" + userInput + "\"}", Encoding.UTF8, "application/json");

            var responseContent = await PostAsync(uri, headers, body);
            return JsonConvert.DeserializeObject<QnaResponse>(responseContent);
        }
    }
}