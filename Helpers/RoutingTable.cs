﻿using System;
using Microsoft.Bot.Connector;

namespace SimpleEchoBot.Helpers
{
    public class User
    {
        public User(IMessageActivity message)
        {
            _toId = message.From.Id;
            _toName = message.From.Name;
            _fromId = message.Recipient.Id;
            _fromName = message.Recipient.Name;
            _serviceUrl = message.ServiceUrl;
            _channelId = message.ChannelId;
            _conversationId = message.Conversation.Id;
            Language = message.Locale?.Substring(0, 2);
        }

        public string _toId { get; set; }
        public string _toName { get; set; }
        public string _fromId { get; set; }
        public string _fromName { get; set; }
        public string _serviceUrl { get; set; }
        public string _channelId { get; set; }
        public string _conversationId { get; set; }
        public string Language { get; set; } = "en";

        protected async void _sendMessage(IMessageActivity msg, IMessageActivity _to = null)
        {
            var userAccount = new ChannelAccount(_toId, _toName);
            var botAccount = new ChannelAccount(_fromId, _fromName);
            var connector = new ConnectorClient(new Uri(_serviceUrl));

            // Create a new message.
            IMessageActivity message = Activity.CreateMessageActivity();
            if (!string.IsNullOrEmpty(_conversationId) && !string.IsNullOrEmpty(_channelId))
            {
                // If conversation ID and channel ID was stored previously, use it.
                message.ChannelId = _channelId;
            }
            else
            {
                // Conversation ID was not stored previously, so create a conversation. 
                // Note: If the user has an existing conversation in a channel, this will likely create a new conversation window.
                _conversationId = (await connector.Conversations.CreateDirectConversationAsync(botAccount, userAccount))
                    .Id;
            }

            // Set the address-related properties in the message and send the message.
            message.From = botAccount;
            message.Recipient = userAccount;
            message.Conversation = new ConversationAccount(id: _conversationId);
            message.Text = msg.Text;
            message.Locale = msg.Locale;
            await connector.Conversations.SendToConversationAsync((Activity) message);
        }

        public async void sendMessage(string text, IMessageActivity returnAddress = null)
        {
            IMessageActivity msg = Activity.CreateMessageActivity();
            msg.Text = text;
            _sendMessage(msg, returnAddress);
        }

        public async void forwardMessage(IMessageActivity msg, string fromPrefix)
        {
            msg.Text = fromPrefix + msg.Text;
            _sendMessage(msg);
        }
    }

    public class RoutingTable
    {
        static RoutingTable _instance = null;

        public User _user { get; set; }
        public User _operator { get; set; }

        public void setOperator(User user)
        {
            _operator = user;
        }

        public void setUser(User user)
        {
            _user = user;
        }

        public bool IsAgentInConverstion(IMessageActivity message)
        {
            return _operator != null && message.From.Id == _operator._toId && _user != null;
        }

        public static RoutingTable instance()
        {
            if (_instance == null)
            {
                _instance = new RoutingTable();
            }

            return _instance;
        }
    }
}