﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using AdaptiveCards;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using SimpleEchoBot.Models;

namespace SimpleEchoBot.Helpers
{
    public static class AdaptiveCardsHelper
    {
        public static AdaptiveCard WelcomeAdaptiveCard(string firstName)
        {
            return new AdaptiveCard()
            {
                Body = new List<CardElement>()
                {
                    new Container()
                    {
                        Speak = $"<s>Hi {firstName}, Welcome to Globostars! Can I help you get started? </s>",
                        Items = new List<CardElement>()
                        {
                            new ColumnSet()
                            {
                                Columns = new List<Column>()
                                {
                                    new Column()
                                    {
                                        Size = ColumnSize.Stretch,
                                        Items = new List<CardElement>()
                                        {
                                            new TextBlock()
                                            {
                                                Text = $"Hi {firstName}, welcome to Globostars!",
                                                Weight = TextWeight.Bolder,
                                                IsSubtle = true
                                            },
                                            new TextBlock()
                                            {
                                                Text = "Can I help you get started?",
                                                Wrap = true
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                // Buttons
                Actions = new List<ActionBase>()
                {
                    new SubmitAction()
                    {
                        Title = "I'd like to recognise my colleague(s)",
                        Data = "I'd like to recognise my colleague(s)",
                        Speak = "I'd like to recognise my colleague(s)",
                    },
                    new SubmitAction()
                    {
                        Title = "Tell me about WorkHuman",
                        Data = "Tell me about WorkHuman",
                        Speak = "Tell me about WorkHuman",
                    },
                    new SubmitAction()
                    {
                        Title = "Tell me more about recognition",
                        Data = "Tell me more about recognition",
                        Speak = "Tell me more about recognition",
                    }
                }
            };
        }

        public static AdaptiveCard PersistenceAdaptiveCard(string firstName, string channelId)
        {
            var x = new AdaptiveCard()
            {
                Body = new List<CardElement>()
                {
                    new Container()
                    {
                        Speak = $"<s>\n So, {firstName},  what else can I help with? </s>",
                        Items = new List<CardElement>()
                        {
                            new ColumnSet()
                            {
                                Columns = new List<Column>()
                                {
                                    new Column()
                                    {
                                        Size = ColumnSize.Auto,
                                        Items = new List<CardElement>()
                                        {
                                            new TextBlock()
                                            {
                                                Text = $"\n So {firstName}, what else can I help you with?",
                                                Weight = TextWeight.Bolder,
                                                IsSubtle = true
                                            },
                                            new TextBlock()
                                            {
                                                Text = "Perhaps you'd like to:",
                                                Wrap = true
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                // Buttons
                Actions = new List<ActionBase>()
                {
                    new SubmitAction()
                    {
                        Title = "Recognise another colleague?",
                        Data = "I'd like to recognise another colleague",
                        Speak = "I'd like to recognise another colleague",
                    },
                    new SubmitAction()
                    {
                        Title = string.Format("Know more about WorkHuman{0}", (channelId == ChannelIds.Slack || channelId == ChannelIds.Emulator) ? string.Empty : " 2019"),
                        Data = "I'd like to know more about WorkHuman",
                        Speak = "I'd like to know more about WorkHuman",
                    },
                    new SubmitAction()
                    {
                        Title = "Know more about the WorkHuman \n Cloud",
                        Data = "I'd like to know more about the WorkHuman Cloud",
                        Speak = "I'd like to know more about the WorkHuman Cloud",
                    }
                }
            };

            if (channelId == ChannelIds.Slack || channelId == ChannelIds.Emulator)
            {
                x.Actions.RemoveAt(2);
            }

            return x;
        }


        public static AdaptiveCard CreateOrderDetailsAdaptiveCard(IMessageActivity message, RedemptionItem r,
            SimpleOrderData sod)
        {
            var formOfDelivery = "";
            var myOrderAddress =
                @"[ here ](https://test-web1-21.corp.globoforce.com/microsites/t/dashboard/MyOrders?client=staff) ";


            var imageUrl = "";

            if (determineOrderType(r) == "Merchandise")
            {
                imageUrl =
                    "https://globosparky.azurewebsites.net/api/img/logos/e6668e9fa6500cdc535449c1aab341752a9b7e61_45572445.jpg";
                r.ProductName = "Home Wares";
            }
            else
            {
                imageUrl = "https://globosparky.azurewebsites.net/api" + r.Logo.Remove(0, 2);
            }

            var persistStoreString = GetStoreString(determineOrderType(r));

            if (r.ChainCode.StartsWith("WWW"))
            {
                formOfDelivery = "e-mail";
            }
            else
            {
                formOfDelivery = Regex.Replace(r.FormOfDeliveryName, "[^A-Za-z0-9 _]", "");
                formOfDelivery = Regex.Replace(formOfDelivery, @"[\d-]", string.Empty);
                formOfDelivery = formOfDelivery.Trim();
            }

            string deliveryInfo;

            switch (formOfDelivery)
            {
                case "e-mail":
                    deliveryInfo =
                        "This was e-mailed to you - please check your inbox or possibly your junk mail folder. \n You can find out more information about your e-Cert order here: \n ";
                    break;
                default:
                    deliveryInfo =
                        "Good news, the item should be with you in 5 days \n If you need more information about your orders click ";
                    break;
            }

            if (message.ChannelId == ChannelIds.Slack || message.ChannelId == ChannelIds.Emulator)
            {
                var deliveryArr = deliveryInfo.Replace("click", "please visit your my orders page.").Split("\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                return new AdaptiveCard()
                {
                    Body = new List<CardElement>()
                    {
                        new Container()
                        {
                            Items = new List<CardElement>()
                            {
                                new ColumnSet()
                                {
                                    Columns = new List<Column>()
                                    {
                                        new Column()
                                        {
                                            Size = ColumnSize.Stretch,
                                            Items = new List<CardElement>()
                                            {
                                                new TextBlock()
                                                {
                                                    Text = deliveryArr[0],
                                                    Weight = TextWeight.Bolder,
                                                    IsSubtle = true,
                                                    Wrap = true
                                                },
                                                new TextBlock()
                                                {
                                                    Text = deliveryArr.Length > 1 ? deliveryArr[1] : string.Empty,
                                                    Weight = TextWeight.Bolder,
                                                    IsSubtle = true,
                                                    Wrap = true
                                                },
                                                new TextBlock()
                                                {
                                                    Text =
                                                        $"The total paid was {r.FormattedFxAmount} and if you need it, the order reference is: {sod.OrderReference}.",
                                                    Weight = TextWeight.Bolder,
                                                    IsSubtle = true,
                                                    Wrap = true
                                                }
                                            }
                                        }
                                    }
                                },
                                new ColumnSet()
                                {
                                    Columns = new List<Column>()
                                    {
                                        new Column()
                                        {
                                            Size = ColumnSize.Stretch,
                                            Items = new List<CardElement>()
                                            {
                                                new TextBlock()
                                                {
                                                    Text = r.ProductName,
                                                    Weight = TextWeight.Bolder,
                                                    IsSubtle = true
                                                },
                                                new Image()
                                                {
                                                    Url = imageUrl,
                                                    Size = ImageSize.Large,
                                                    Style = ImageStyle.Normal
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                };
            }
            deliveryInfo = deliveryInfo + (myOrderAddress + "\n");

            var orderMessage =
                $"{deliveryInfo} The total paid was {r.FormattedFxAmount} and if you need it, the order reference is: {sod.OrderReference}.";

            return new AdaptiveCard()
            {
                Body = new List<CardElement>()
                {
                    new Container()
                    {
                        Items = new List<CardElement>()
                        {
                            new ColumnSet()
                            {
                                Columns = new List<Column>()
                                {
                                    new Column()
                                    {
                                        Size = ColumnSize.Auto,
                                        Items = new List<CardElement>()
                                        {
                                            new TextBlock()
                                            {
                                                Text = orderMessage,
                                                Weight = TextWeight.Bolder,
                                                IsSubtle = true,
                                                Wrap = true
                                            }
                                        }
                                    }
                                }
                            },
                            new ColumnSet()
                            {
                                Columns = new List<Column>()
                                {
                                    new Column()
                                    {
                                        Size = ColumnSize.Stretch,
                                        Items = new List<CardElement>()
                                        {
                                            new TextBlock()
                                            {
                                                Text = r.ProductName,
                                                Weight = TextWeight.Bolder,
                                                IsSubtle = true
                                            },
                                            new Image()
                                            {
                                                Url = imageUrl,
                                                Size = ImageSize.Large,
                                                Style = ImageStyle.Normal
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };
        }

        private static string determineOrderType(RedemptionItem r)
        {
            var orderType = "";

            if (r.ChainCode.StartsWith("WWW") || r.ChainCode.StartsWith("URL"))
            {
                orderType = "Ecert";
            }
            else if (r.ChainCode.StartsWith("IRL"))
            {
                orderType = "GiftCard";
            }
            else orderType = "Merchandise";


            return orderType;
        }

        private static string GetStoreString(string store)
        {
            string returnString = "";

            switch (store)
            {
                case "Ecert":
                    returnString = "Here is the relevant link to the ecert store";
                    break;
                case "GiftCard":
                    returnString = "Here is the relevant link to the giftcard store";
                    break;
                case "Merchandise":

                    var globoStoreAddress =
                        @"[ House and Home ]( https://test-web1-21.corp.globoforce.com/microsites/t/redeem/rewardCategories?selectedCategoryID=34) ";

                    returnString =
                        $"In the meantime, why not browse the newest additions to {globoStoreAddress} within the GloboStore - there are some great new offers!";
                    break;
            }

            return returnString;
        }
    }
}