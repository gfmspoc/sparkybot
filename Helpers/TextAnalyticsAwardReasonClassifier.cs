﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Azure.CognitiveServices.Language.TextAnalytics;
using Microsoft.Azure.CognitiveServices.Language.TextAnalytics.Models;
using Microsoft.Rest;
using SimpleEchoBot.Models;
using SimpleEchoBot.Models.AwardReasons;

namespace SimpleEchoBot.Helpers
{
    /**
     * 
     * <summary>
     * Concrete implementation of an IAwardReasonClassifier that runs its input
     * through the Azure Text Analytics API in order to extract key phrases.
     * Then, those key phrases are mapped to known Globoforce award reason
     * values.
     * </summary>
     * 
     * <example>
     * var classifier = new TextAnalyticsAwardReasonClassifier();
     * classifier.Client = new TextAnalyticsClient(new ApiKeyServiceClientCredentials());
     * classifier.Matcher = new GloboforceAwardReasonMatcher();
     * </example>
     * 
     */
    public class TextAnalyticsAwardReasonClassifier : IAwardReasonClassifier
    {
        public TextAnalyticsAwardReasonClassifier()
        {
            var credentials = new TextAnalyticsServiceClientCredentials();
            Client = new TextAnalyticsClient(credentials);
            Client.BaseUri = new Uri(credentials.GetBaseUri());

            Matcher = new GloboforceAwardReasonMatcher();
        }

        public ITextAnalyticsClient Client { get; set; }
        public IAwardReasonMatcher Matcher { get; set; }

        public async Task<IList<AwardReason>> ClassifyAwardReasons(string text, Dictionary<string, object> context)
        {
            List<MultiLanguageInput> documents = new List<MultiLanguageInput>()
            {
                new MultiLanguageInput("en", "1", text)
            };

            HttpOperationResponse<KeyPhraseBatchResult> result =
                await Client.KeyPhrasesWithHttpMessagesAsync(new MultiLanguageBatchInput(documents));
            if (result == null || result.Body == null)
            {
                // TODO: Handle error cases more explicitly instead of failing silently with an empty result.
                return new List<AwardReason>();
            }

            if (result.Body.Documents.Count == 0)
            {
                return new List<AwardReason>();
            }

            // Because the input provided to the client was a single input document, as single response document is expected.
            IList<string> phrases = result.Body.Documents[0].KeyPhrases;
            IList<AwardReason> reasons = MatchGlobalReasons(phrases, context);
            return await Matcher.MatchAwardReasons(reasons, context);
        }

        private IList<AwardReason> MatchGlobalReasons(IList<string> phrases, Dictionary<string, object> context)
        {
            return new DummyReasons().GetReasons(phrases);
        }
    }
}