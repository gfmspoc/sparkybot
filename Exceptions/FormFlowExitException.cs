﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleEchoBot.Exceptions
{
    public class  FormFlowExitException :Exception
    {
        public FormFlowExitException()
        {
        }

        public FormFlowExitException(string message)
        : base(message)
    {
        }

        public FormFlowExitException(string message, Exception inner)
        : base(message, inner)
    {
        }
    }
}
