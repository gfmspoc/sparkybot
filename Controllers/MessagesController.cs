using AdaptiveCards;
using Microsoft.Bot.Builder.ConnectorEx;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Connector;
using Newtonsoft.Json;
using SimpleEchoBot;
using SimpleEchoBot.Dialogs;
using SimpleEchoBot.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SimpleEchoBot.Forms;
using SimpleEchoBot.Models;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;

namespace Microsoft.Bot.Sample.SimpleEchoBot
{
    [BotAuthentication]
    public class MessagesController : ApiController
    {

        private string _intendedRecepiant = "";

        /// <summary>
        /// POST: api/Messages
        /// receive a message from a user and send replies
        /// </summary>
        /// <param name="activity"></param>
        [ResponseType(typeof(void))]
        public virtual async Task<HttpResponseMessage> Post([FromBody] Activity activity)
        {
            // check if activity is of type message
            if (activity != null && activity.GetActivityType() == ActivityTypes.Message)
            {
                var routingTable = RoutingTable.instance();
                if (routingTable.IsAgentInConverstion(activity))
                {
                    await Conversation.SendAsync(activity, () => new OperatorDialog());
                }
                else
                {
                    try
                    {
                        await Conversation.SendAsync(activity, () => MakeRootDialog());
                    }
                    catch (FormCanceledException<NominationFormDirectline> ex)
                    {
                        TrackEventHelper.TrackEvent("Nomination", ex.Last, activity);
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
            }
            else
            {
                
                    await HandleSystemMessage(activity);
               
            }

            return new HttpResponseMessage(System.Net.HttpStatusCode.Accepted);
        }

        class ChannelUser
        {
            public string Id { get; set; }
            public string Name { get; set; }
        }

        class DirectMessage
        {
            public ChannelUser Recipient { get; set; }
            public ChannelUser Bot { get; set; } = new ChannelUser { Id = "BBUAT8BNK:TBQD5CL9W", Name = "sparky" };
            public string Message { get; set; }

        }
        private static async Task SendSlackInvite(Activity activity, DirectMessage data)
        {

            string slackServiceUrl = "https://slack.botframework.com/";

            try
            {
                MicrosoftAppCredentials.TrustServiceUrl(slackServiceUrl, DateTime.Now.AddDays(7));
                var account = new MicrosoftAppCredentials(ConfigurationManager.AppSettings["MicrosoftAppId"], ConfigurationManager.AppSettings["MicrosoftAppPassword"]);
                ConnectorClient connector = new ConnectorClient(new Uri(slackServiceUrl), account);
                var userAccount = new ChannelAccount(data.Recipient.Id, data.Recipient.Name);
                var botAccount = new ChannelAccount(data.Bot.Id, data.Bot.Name);
                //""UBT9RN64A:TBQD5CL9W //this bot account is not found
                var conversation = await connector.Conversations.CreateDirectConversationAsync(botAccount, userAccount);

                IMessageActivity reply = Activity.CreateMessageActivity();
                reply.Type = ActivityTypes.Message;
                reply.From = botAccount;
                reply.Recipient = userAccount;
                reply.Conversation = new ConversationAccount(id: conversation.Id);
                reply.Text = data.Message ?? "Hi! This is Sparky";
                reply.ChannelId = "slack";
                reply.ServiceUrl = slackServiceUrl;

                await connector.Conversations.SendToConversationAsync((Activity)reply);
            }
            catch (Exception ex)
            {
                ConnectorClient connector = new ConnectorClient(new Uri(activity.ServiceUrl));
                await connector.Conversations.ReplyToActivityAsync(activity.CreateReply($"Error: {ex.Message}"));
            }
        }
        internal static IDialog<object> MakeRootDialog()
        {
            return Chain.From(() => new LUISDialog());
        }

        void storeUserState(IMessageActivity message)
        {
            var conversation = message.ToConversationReference();
            ConversationStarter.SaveConverstion(message.From.Id, JsonConvert.SerializeObject(conversation));
        }
        private async Task<Activity> HandleSystemMessage(Activity message)
        {
            ConnectorClient connector = new ConnectorClient(new Uri(message.ServiceUrl));
            if (message.Type == ActivityTypes.DeleteUserData)
            {
                // Implement user deletion here
                // If we handle user deletion, return a real message
            }
            else if (message.Type == ActivityTypes.ConversationUpdate)
            {
                // Handle conversation state changes, like members being added and removed
                // Use Activity.MembersAdded and Activity.MembersRemoved and Activity.Action for info
                // Not available in all channels
                if (message.ChannelId == ChannelIds.Directline) return null;

                if (message.MembersAdded.Any(o => o.Id == message.Recipient.Id))
                {
                    var reply = CreateWelcomeCard(message);
                    await connector.Conversations.ReplyToActivityAsync(reply);
                }
            }
            else if (message.Type == ActivityTypes.ContactRelationUpdate)
            {
                // Handle add/remove from contact lists
                // Activity.From + Activity.Action represent what happened
            }
            else if (message.Type == ActivityTypes.Typing)
            {

            }
            else if (message.Type == ActivityTypes.Ping)
            {
                if (message.Value != null)
                {
                    var data = JsonConvert.DeserializeObject<DirectMessage>(message.Value.ToString());
                    await connector.Conversations.ReplyToActivityAsync(message.CreateReply($"Sending message to {data.Recipient.Name}"));
                    await SendSlackInvite(message, data);
                }
            }
            else if (message.Type == ActivityTypes.Event)
            {
                var routingTable = RoutingTable.instance();

                switch (message.AsEventActivity().Name)
                {
                    case "requestWelcome":
                        var reply = CreateWelcomeCard(message);
                        await connector.Conversations.ReplyToActivityAsync(reply);
                        break;
                    case "requestAnalysis":
                        var r = message.CreateReply();
                        r.Type = "event";
                        var analyzer = new AwardAnalyzer();
                        var language = await analyzer.DetectLanguage(message.Text);
                        var context = new Dictionary<string, object>();
                        if (language != null)
                        {
                            context.Add("language", language);
                        }
                        var analysis = await analyzer.AnalyzeAwardMessageAsync(message.Text, context);
                        r.Name = BotEvents.PositivityRating;
                        r.Value = JsonConvert.SerializeObject(analysis);
                        await connector.Conversations.SendToConversationAsync(r);
                        break;
                    case "connect":
                        routingTable.setOperator(new User(message));
                        await connector.Conversations.ReplyToActivityAsync(message.CreateReply("Ok you are now an Agent"));
                        break;
                    case "disconnect":
                        if (routingTable._user != null)
                        {
                            message.Text = "Operator has disconnected";
                            routingTable._user.forwardMessage(message, "");
                        }
                        routingTable._operator = null;
                        await connector.Conversations.ReplyToActivityAsync(message.CreateReply("You have disconnected"));

                        break;
                    case "stopConversationAgent":
                        await connector.Conversations.ReplyToActivityAsync(message.CreateReply("You have ended the conversation"));
                        if (routingTable._user != null)
                        {
                            message.Text = "Agent disconnected";
                            routingTable._user.forwardMessage(message, "");
                        }
                        routingTable._operator = null;
                        break;
                    case "stopConversationUser":
                        await connector.Conversations.ReplyToActivityAsync(message.CreateReply("You have ended the conversation"));
                        if (routingTable._operator != null)
                        {
                            message.Text = "User disconnected";
                            routingTable._operator.forwardMessage(message, "");
                        }
                        routingTable._operator = null;
                        break;

                    case "NominationStep Selected Users":
                        _intendedRecepiant = ReturnNamesAsSentence(message.Value.ToString());
                        break;

                    case "NominationStep select-recipients":
                        _intendedRecepiant = ReturnNamesAsSentence(message.Value.ToString());
                        await connector.Conversations.ReplyToActivityAsync(message.CreateReply(String.Format(UserStrings.NominationStepSelectRecepients, GetFirstName(message.From.Name.ToString()))));
                        break;

                    case "NominationStep select-programs":
                        _intendedRecepiant = ReturnNamesAsSentence(message.Value.ToString());
                        await connector.Conversations.ReplyToActivityAsync(message.CreateReply(String.Format(UserStrings.NominationStepNominationSelectPrograms, _intendedRecepiant)));
                        break;

                    case "NominationStep select-reason":
                        _intendedRecepiant = ReturnFirstNamesAsSentence(message.Value.ToString());

                        var messageString = UserStrings.NominationStepSelectReason;
                        if (checkForMultipleNames(_intendedRecepiant))
                        {
                            messageString = messageString.Replace("has", "have");
                        }

                        await connector.Conversations.ReplyToActivityAsync(message.CreateReply(String.Format(messageString, _intendedRecepiant)));
                        break;

                    case "NominationStep select-award":
                        _intendedRecepiant = ReturnFirstNamesAsSentence(message.Value.ToString());
                        await connector.Conversations.ReplyToActivityAsync(message.CreateReply(String.Format(UserStrings.NominationStepSelectAward, _intendedRecepiant)));
                        break;

                    case "NominationStep nomination-message":
                        //use first names
                        _intendedRecepiant = ReturnFirstNamesAsSentence(message.Value.ToString());
                        await connector.Conversations.ReplyToActivityAsync(message.CreateReply(String.Format(UserStrings.NominationStepNominationMessage, _intendedRecepiant)));
                        break;

                    case "NominationStep created-notification":

                        //use first names
                        _intendedRecepiant = ReturnFirstNamesAsSentence(message.Value.ToString());

                        var nominationCreatedMessageString = UserStrings.NominationStepCreatedNotification;

                        if (checkForMultipleNames(_intendedRecepiant))

                        {
                            nominationCreatedMessageString = nominationCreatedMessageString.Replace("colleague", "colleagues");
                        }

                        await connector.Conversations.ReplyToActivityAsync(message.CreateReply(String.Format(nominationCreatedMessageString, GetFirstName(message.From.Name.ToString()), _intendedRecepiant)));

                        var persistReply = CreatePersistenceCard(message);
                        await connector.Conversations.ReplyToActivityAsync(persistReply);

                        break;


                    case "responseMyOrders":

                        var myOrders = CreateOrderObjects(message.Value.ToString());

                        if (myOrders == null || myOrders.Orders.Count() < 1)
                        {
                            var myOrderAddress = @"[ GloboStore ]( https://test-web1-21.corp.globoforce.com/microsites/t/awards/Redeem?client=staff) ";
                           

                            var o = message.CreateReply();
                            o.Type = "message";
                            o.Text = String.Format( "It seems that you haven't placed an order lately. \n Why don't you visit the {0} to see our great offers.", myOrderAddress);
                            await connector.Conversations.ReplyToActivityAsync(o);
                        }
                        else
                        {
                            var sod = new SimpleOrderData();

                            sod.OrderReference = myOrders.Orders[0].OrderReference ?? "Order Number not found";
                            sod.FormattedCreatedDate = myOrders.Orders[0].FormattedCreatedDate ?? "Order Create Date not found";
                            sod.FormOfDelivery = determineFOD(myOrders);

                            var orders = returnOrdersFromobject(myOrders);
                            var orderDetailString = returnOrderinfo(orders);

                         //  var persistReplyMyOrders = CreatePersistenceCard(message);


                             var orderreply = CreateOrderCard(message, orders, sod);

                             var persistStoreString = GetStoreString( determineOrderType(myOrders));

                         
                            await connector.Conversations.ReplyToActivityAsync(orderreply);
                            //await connector.Conversations.ReplyToActivityAsync(CreatePersistenceCard(message));


                            var o = message.CreateReply();
                            o.Type = "message";
                            o.Text = persistStoreString;
                            await connector.Conversations.ReplyToActivityAsync(o);
                            

                        }
                        break;

                    default:
                        break;
                }

            }
            return null;
        }

        private string determineOrderType(MyOrders mo)
        {
            var orderType = "Merchandise";

            RedemptionItem r = mo.Orders[0].RedemptionItems[0];


            if (r.ChainCode.StartsWith("WWW") || r.ChainCode.StartsWith("URL"))
            {
                orderType = "Ecert";
            }
            else if (r.ChainCode.StartsWith("IRL"))
            {
                orderType = "GiftCard";

            }
            
            
         
            return orderType;
        }

       private string GetStoreString(string store)
        {
            string returnString = "";

            switch (store)
            {
                case "Ecert":
                    returnString = "Here is the relevant link to the ecert store";
                    break;
                case "GiftCard":
                    returnString = "Here is the relevant link to the giftcard store";
                    break;
                case "Merchandise":
                    var globoStoreAddress = @"[ House and Home ]( https://test-web1-21.corp.globoforce.com/microsites/t/redeem/rewardCategories?selectedCategoryID=34) ";
                   returnString = String.Format("In the meantime, why not browse the newest additions to {0} within the GloboStore - there are some great new offers!", globoStoreAddress) ;
                     break;
            }


            return returnString;
        }


    private string determineFOD(MyOrders mo)
        {
            var formOfDelivery = "";

            RedemptionItem r = mo.Orders[0].RedemptionItems[0];
            
            if (r.ChainCode.StartsWith("WWW") || r.ChainCode.StartsWith("URL"))
            {
                formOfDelivery = "ecert";
            }
            else
            {

                formOfDelivery = Regex.Replace(r.FormOfDeliveryName, "[^A-Za-z0-9 _]", "");
                formOfDelivery = Regex.Replace(formOfDelivery, @"[\d-]", string.Empty);
                formOfDelivery = formOfDelivery.Trim();
            }

           

            return formOfDelivery;
        }

        #region activityCard

        private Activity CreateOrderCard(Activity message, List <RedemptionItem> ri, SimpleOrderData sod)
        {
            Activity reply = message.CreateReply();

            reply.Attachments = new List<Attachment>();

            foreach (RedemptionItem r in ri)
            {

                var card = AdaptiveCardsHelper.CreateOrderDetailsAdaptiveCard(message, r, sod);

                reply.Attachments.Add(new Attachment()
                {
                    ContentType = AdaptiveCard.ContentType,
                    Content = card
                });
            }
            return reply;
        }


        private Activity CreateWelcomeCard(Activity message)
        {
            Activity reply = message.CreateReply();

            string firstName = GetFirstName(message.From.Name);

            reply.Attachments = new List<Attachment>();
            var card = AdaptiveCardsHelper.WelcomeAdaptiveCard(firstName);

            reply.Attachments.Add(new Attachment()
            {
                ContentType = AdaptiveCard.ContentType,
                Content = card
            });

            return reply;
        }

        private Activity CreatePersistenceCard(Activity message)
        {
            Activity reply = message.CreateReply();
            string firstName = GetFirstName(message.From.Name);
            reply.Attachments = new List<Attachment>();
            var card = AdaptiveCardsHelper.PersistenceAdaptiveCard(firstName, message.ChannelId);

            reply.Attachments.Add(new Attachment()
            {
                ContentType = AdaptiveCard.ContentType,
                Content = card
            });

            return reply;
        }

        #endregion

        #region orderObjects

        private static MyOrders CreateOrderObjects(string orderJSon)
        {
            if (string.IsNullOrWhiteSpace(orderJSon)) return null;

            MyOrders myOrders = new JavaScriptSerializer().Deserialize<MyOrders>(orderJSon);

            return myOrders;
        }

        private static string returnOrderinfo(List<RedemptionItem> list )
        {

            string returnString = "";

            foreach(RedemptionItem r in list)
            {
              
                returnString +=  $"\n" +
               "Product Name:" + r.ProductName + "\n" +
               "Processed Date" + r.ProcessedDate.ToString() +
                r.Logo + "\n";

            }
           
               
            return returnString;
        }

        private static List<RedemptionItem> returnOrdersFromobject(MyOrders o)
        {

            RedemptionItem r = new RedemptionItem();

            List<RedemptionItem> rList = new List<RedemptionItem>();
            rList.Add(o.Orders[0].RedemptionItems[0]);


            //            r.ProductName =  o.Orders[0].RedemptionItems[0].ProductName;
            //          r.ProcessedDate = o.Orders[0].RedemptionItems[0].ProcessedDate;
            //        r.Logo =         o.Orders[0].RedemptionItems[0].Logo;

            return rList;

        }


        #endregion

        #region nameString trickery
        private static string GetFirstName(string name)
        {
            if (string.IsNullOrWhiteSpace(name)) return "Globo Buddy";

            return name.Trim().Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[0];
        }
      
        private static string ReturnNamesAsSentence(string namesCommaSeperated)
        {
            string find = ",";
            string replace = " and ";
            int place = namesCommaSeperated.LastIndexOf(find);
            string result = namesCommaSeperated;
            if (place > 0)
            {
                result = namesCommaSeperated.Remove(place, find.Length).Insert(place, replace);
            }
            return result;
        }

        private static string ReturnFirstNamesAsSentence(string namesCommaSeperated)
        {
            var names = namesCommaSeperated.Split(',');
            return ReturnNamesAsSentence(string.Join(", ", names.Select(GetFirstName)));
        }

        private static bool checkForMultipleNames(string namesToCheck)
        {
            return namesToCheck.Contains(" and ");
        }

    #endregion
       
    }
}