﻿using Microsoft.Azure.CognitiveServices.Language.TextAnalytics;
using Microsoft.Azure.CognitiveServices.Language.TextAnalytics.Models;
using SimpleEchoBot.Helpers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SimpleEchoBot.Services
{
    public class TextAnalyticsService
    {

        public ITextAnalyticsClient _client { get; set; }
        public TextAnalyticsService()
        {
            var credentials = new TextAnalyticsServiceClientCredentials();
            _client = new TextAnalyticsClient(credentials);
            _client.BaseUri = new Uri(credentials.GetBaseUri());
        }

        public async Task<SentimentBatchResultItem> GetSentiment(string text, string locale = "en")
        {
            try
            {
                MultiLanguageBatchInput input = CreateMultiLanguageInput(text, locale);
                var result = await _client.SentimentWithHttpMessagesAsync(input);
                if (result == null || result.Body == null || result.Body.Documents == null ||
                    result.Body.Documents.Count == 0)
                {
                    return null;
                }

                return result.Body.Documents[0];
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
                return null;
            }
        }

        MultiLanguageBatchInput CreateMultiLanguageInput(string text, string language)
        {
            List<MultiLanguageInput> documents = new List<MultiLanguageInput>()
            {
                new MultiLanguageInput(language, "1", text)
            };
            return new MultiLanguageBatchInput(documents);
        }

    }
}