﻿using Microsoft.Azure.CognitiveServices.Language.TextAnalytics;
using Microsoft.Azure.CognitiveServices.Language.TextAnalytics.Models;
using Microsoft.Rest;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SimpleEchoBot.Helpers;
using SimpleEchoBot.Models;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SparkyTests.Models
{
    [TestClass]
    public class AwardAnalyzerTests
    {
        private AwardAnalyzer analyzer;
        private Mock<ITextAnalyticsClient> client;
        private Mock<IAwardReasonMatcher> matcher;

        [TestInitialize]
        public void setUp()
        {
            this.client = new Mock<ITextAnalyticsClient>();
            this.client.Setup(
                c => c.KeyPhrasesWithHttpMessagesAsync(It.IsAny<MultiLanguageBatchInput>(), null, CancellationToken.None)
            ).Returns(
                Task.FromResult<HttpOperationResponse<KeyPhraseBatchResult>>(null)
            );
            this.client.Setup(
                c => c.SentimentWithHttpMessagesAsync(It.IsAny<MultiLanguageBatchInput>(), null, CancellationToken.None)
            ).Returns(
                Task.FromResult<HttpOperationResponse<SentimentBatchResult>>(null)
            );

            this.matcher = new Mock<IAwardReasonMatcher>();
            this.matcher.Setup(
                m => m.MatchAwardReasons(It.IsAny<IList<AwardReason>>(), It.IsAny<Dictionary<string, object>>())
            ).Returns<IList<AwardReason>, Dictionary<string, object>>(
                (reasonsInput, contextInput) => Task.FromResult(reasonsInput)
            );

            this.analyzer = new AwardAnalyzer();
            this.analyzer.Client = this.client.Object;
            this.analyzer.Matcher = this.matcher.Object;
        }

        [TestMethod]
        public void TestEmptyResults()
        {
            var analysis = this.analyzer.AnalyzeAwardMessageAsync("Lizzy was awesome. She helped save the project.", new Dictionary<string, object>());
            Assert.IsNotNull(analysis);
            Assert.AreEqual(0.0, analysis.Result.PositiviyScore, 0.1);
            Assert.IsNotNull(analysis.Result.Reasons);
            Assert.AreEqual(0, analysis.Result.Reasons.Count);
        }

        [TestMethod]
        public void TestPositivityScorePresent()
        {
            var sentiment = new SentimentBatchResult(
                new List<SentimentBatchResultItem>()
                {
                    new SentimentBatchResultItem(0.75)
                },
                null
            );
            var response = new HttpOperationResponse<SentimentBatchResult>();
            response.Body = sentiment;

            this.client.Setup(
                c => c.SentimentWithHttpMessagesAsync(It.IsAny<MultiLanguageBatchInput>(), null, CancellationToken.None)
            ).Returns(
                Task.FromResult<HttpOperationResponse<SentimentBatchResult>>(response)
            );


            var analysis = this.analyzer.AnalyzeAwardMessageAsync("Lizzy was awesome. She helped save the project.", new Dictionary<string, object>());
            Assert.IsNotNull(analysis);
            Assert.AreEqual(0.254, analysis.Result.PositiviyScore, 0.01);
            Assert.IsNotNull(analysis.Result.Reasons);
            Assert.AreEqual(0, analysis.Result.Reasons.Count);
        }

        [TestMethod]
        public void TestKeyphraseResultsPresent()
        {
            var response = new HttpOperationResponse<KeyPhraseBatchResult>
            {
                Body = new KeyPhraseBatchResult(new List<KeyPhraseBatchResultItem>() {
                    new KeyPhraseBatchResultItem(new List<string>(){
                        "create",
                        "develop",
                        "collaboration"
                    })
                })
            };

            this.client.Setup(
                c => c.KeyPhrasesWithHttpMessagesAsync(It.IsAny<MultiLanguageBatchInput>(), null, CancellationToken.None)
            ).Returns(
                Task.FromResult<HttpOperationResponse<KeyPhraseBatchResult>>(response)
            );

            var analysis = this.analyzer.AnalyzeAwardMessageAsync(
                "some text endered by the user",
                new Dictionary<string, object>()
            );

            Assert.IsNotNull(analysis);
            Assert.IsNotNull(analysis.Result);

            IList<AwardReason> reasons = analysis.Result.Reasons;
            Assert.IsNotNull(reasons);
            Assert.AreEqual(2, reasons.Count);
            Assert.AreEqual("innovation", reasons[0].Value);
            Assert.AreEqual(0.5, reasons[0].Score, 0.01);
            Assert.AreEqual("customer service", reasons[1].Value);
            Assert.AreEqual(0.5, reasons[1].Score, 0.01);
        }

        [TestMethod]
        public void TestCalculatePositivityScoreBaseline()
        {
            Assert.AreEqual(
                0.458,
                analyzer.CalculatePositivityScore(
                    "one two three four five six seven eight nine ten",
                    new List<string>() { "team", "innovation", "imagination" },
                    new List<AwardReason>() { new AwardReason() { Value = "team work", Score = (float)0.55 } },
                    new SentimentBatchResultItem(0.45),
                    new Dictionary<string, object>()
                ),
                0.01
            );
        }

        [TestMethod]
        public void TestCalculatePositivityScoreMonsterMessage()
        {
            Assert.AreEqual(
                0.625,
                analyzer.CalculatePositivityScore(
                    "I would like to acknowledge the simple fact that both of you are on the very first \"front\" line for our company for our clients and the employees, and both of you are doing the job with such a grace! The number of characters and the extent of the inquiries must be vast, but you are managing them all equally and professionally, and this smooth running of the office seems natural for everyone. I personally had a chance to see your work practically first hand when you were dealing with such a complicated issues like parking. As our company grows, the convenient parking is becoming a problem, and some people are taking a \"shortcut\" leaving their cars in places not designed for the purpose. Both of you had to face ever increasing the number of complaints from different people, had to try to reason with the repetitive offenders over and over again, while looking for a fair compromise when the security came over to clamp. At the same time you provided comprehensive information on that and great many other office-related issues, always very accurately and professionally! That example was only a recent happening, and there were many other issues were dealing in past , and everyone always appreciate all the good job you are doing!",
                    new List<string>() { "team", "innovation", "imagination" },
                    new List<AwardReason>() { new AwardReason() { Value = "team work", Score = (float)0.55 } },
                    new SentimentBatchResultItem(0.45),
                    new Dictionary<string, object>()
                ),
                0.001
            );

        }

        [TestMethod]
        public void TestCalculatePositivityScoreByKeyPhrases()
        {
            Assert.AreEqual(
                0.583,
                analyzer.CalculatePositivityScore(
                    "one two three four five six seven eight nine ten",
                    new List<string>() { "team", "innovation", "imagination", "positive", "hard work", "outstanding", "good job" },
                    new List<AwardReason>() { new AwardReason() { Value = "team work", Score = (float)0.55 } },
                    new SentimentBatchResultItem(0.45),
                    new Dictionary<string, object>()
                ),
                0.001
            );

        }

        [TestMethod]
        public void TestCalculatePositivityScoreByAwardReason()
        {
            Assert.AreEqual(
                0.57,
                analyzer.CalculatePositivityScore(
                    "one two three four five six seven eight nine ten",
                    new List<string>() { "team", "innovation", "imagination" },
                    new List<AwardReason>() { new AwardReason() { Value = "team work", Score = (float)1.0 } },
                    new SentimentBatchResultItem(0.45),
                    new Dictionary<string, object>()
                ),
                0.01
            );
        }

        [TestMethod]
        public void TestCalculatePositivityScoreBySentiment()
        {
            Assert.AreEqual(
                0.596,
                analyzer.CalculatePositivityScore(
                    "one two three four five six seven eight nine ten",
                    new List<string>() { "team", "innovation", "imagination" },
                    new List<AwardReason>() { new AwardReason() { Value = "team work", Score = (float)0.55 } },
                    new SentimentBatchResultItem(1.0),
                    new Dictionary<string, object>()
                ),
                0.001
            );
        }

    }
}
