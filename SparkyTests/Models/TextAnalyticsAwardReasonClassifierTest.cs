﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleEchoBot.Helpers;
using Moq;
using Microsoft.Azure.CognitiveServices.Language.TextAnalytics;
using Microsoft.Azure.CognitiveServices.Language.TextAnalytics.Models;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Rest;
using SimpleEchoBot.Models;


namespace SparkyTests
{
    [TestClass]
    public class TextAnalyticsAwardReasonClassifierTest
    {
        private TextAnalyticsAwardReasonClassifier classifier;
        private Mock<ITextAnalyticsClient> mockClient;
        private Mock<IAwardReasonMatcher> matcher;

        [TestInitialize]
        public void setUp()
        {
            this.classifier = new TextAnalyticsAwardReasonClassifier();

            this.mockClient = new Mock<ITextAnalyticsClient>();
            this.mockClient.Setup(
                c => c.KeyPhrasesWithHttpMessagesAsync(It.IsAny<MultiLanguageBatchInput>(), null, CancellationToken.None)
            ).Returns(
                Task.FromResult<HttpOperationResponse<KeyPhraseBatchResult>>(null)
            );
            this.classifier.Client = this.mockClient.Object;

            this.matcher = new Mock<IAwardReasonMatcher>();
            this.matcher.Setup(
                m => m.MatchAwardReasons(It.IsAny<IList<AwardReason>>(), It.IsAny<Dictionary<string, object>>())
            ).Returns(
                Task.FromResult<IList<AwardReason>>(null)
            );
            this.classifier.Matcher = this.matcher.Object;

        }
        [TestMethod]
        public void TestInitialState()
        {
            Assert.IsNotNull(this.classifier.Client);
            Assert.IsNotNull(this.classifier.Matcher);
        }

        [TestMethod]
        public void TestEmptyClassification()
        {
            var classification = this.classifier.ClassifyAwardReasons(
                "some text endered by the user",
                new Dictionary<string, object>()
            );
            Assert.IsNotNull(classification);

            IList<AwardReason> reasons = classification.Result;
            Assert.IsNotNull(reasons);
            Assert.AreEqual(reasons.Count, 0);
        }

        [TestMethod]
        public void TestWithKeyphraseResults()
        {
            var response = new HttpOperationResponse<KeyPhraseBatchResult>
            {
                Body = new KeyPhraseBatchResult(new List<KeyPhraseBatchResultItem>() {
                    new KeyPhraseBatchResultItem(new List<string>(){
                        "create",
                        "develop",
                        "collaboration"
                    })
                })
            };

            this.mockClient.Setup(
                c => c.KeyPhrasesWithHttpMessagesAsync(It.IsAny<MultiLanguageBatchInput>(), null, CancellationToken.None)
            ).Returns(
                Task.FromResult<HttpOperationResponse<KeyPhraseBatchResult>>(response)
            );

            this.matcher.Setup(
                m => m.MatchAwardReasons(It.IsAny<IList<AwardReason>>(), It.IsAny<Dictionary<string, object>>())
            ).Returns<IList<AwardReason>, Dictionary<string, object>>(
                (reasonsInput, contextInput) => Task.FromResult(reasonsInput)
            );


            var classification = this.classifier.ClassifyAwardReasons(
                "some text endered by the user",
                new Dictionary<string, object>()
            );
            Assert.IsNotNull(classification);

            IList<AwardReason> reasons = classification.Result;
            Assert.IsNotNull(reasons);
            Assert.AreEqual(2, reasons.Count);
            Assert.AreEqual("innovation", reasons[0].Value);
            Assert.AreEqual(0.5, reasons[0].Score, 0.01);
            Assert.AreEqual("customer service", reasons[1].Value);
            Assert.AreEqual(0.5, reasons[1].Score, 0.01);
        }

        /*
        [TestMethod]
        public void testIntegration()
        {
            this.classifier = new TextAnalyticsAwardReasonClassifier();
            var classification = this.classifier.ClassifyAwardReasons(
                "Lizzy was amazing. However I still don't have my award.",
                new Dictionary<string, object>()
            );
            Assert.IsNotNull(classification);
            IList<AwardReason> reasons = classification.Result;
            Assert.IsNotNull(reasons);
        }
        */
    }
}
