﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Internals;
using Microsoft.Bot.Connector;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace SimpleEchoBot
{
    public class ConversationStarter
    {
        private static Dictionary<string, string> conversationReference = new Dictionary<string, string>();

        public static void SaveConverstion(string userId, string conversation)
        {
            if (conversationReference.ContainsKey(userId))
            {
                conversationReference[userId] = conversation;
            }
            else
            {
                conversationReference.Add(userId, conversation);
            }
        }

        //This will interrupt the conversation and send the user to SurveyDialog, then wait until that's done 
        public static async Task Resume(string userId)
        {
            if (!conversationReference.ContainsKey(userId)) return;

            var message = JsonConvert.DeserializeObject<ConversationReference>(conversationReference[userId]).GetPostToBotMessage();
            var client = new ConnectorClient(new Uri(message.ServiceUrl));

            message.Text = "Hello, this is a notification";
            message.Locale = "en-Us";
            await client.Conversations.SendToConversationAsync((Activity)message);
        }
    }
}