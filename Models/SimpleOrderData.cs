﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleEchoBot.Models
{
    public class SimpleOrderData
    {
        public string SaleType { get; set; }
        public string FormOfDelivery { get; set; }
        public string FormattedCreatedDate { get; set; }
        public string OrderReference { get; set; }

    }
}
