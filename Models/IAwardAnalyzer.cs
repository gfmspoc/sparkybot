using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SimpleEchoBot.Models
{
    public interface IAwardAnalyzer
    {
        Task<AwardAnalysis> AnalyzeAwardMessageAsync(string text, Dictionary<string, object> context);
        AwardAnalysis AnalyzeAwardMessage(string text, Dictionary<string, object> context);
        Task<string> DetectLanguage(string text);
    }

    [Serializable]
    public class AwardAnalysis
    {
        public double PositiviyScore { get; set; }
        public IList<AwardReason> Reasons { get; set; }
        public double WordCountScore { get; set; }
        public IList<string> KeyPhrases { get; set; }
    }

    [Serializable]
    public class AwardReason
    {
        public string Value { get; set; }

        public float Score { get; set; }

        public IList<Tuple<int, int>> InputOffsets { get; set; }
    }
}