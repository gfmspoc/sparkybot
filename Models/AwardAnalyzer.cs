﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure.CognitiveServices.Language.TextAnalytics;
using Microsoft.Azure.CognitiveServices.Language.TextAnalytics.Models;
using Microsoft.Rest;
using SimpleEchoBot.Helpers;
using SimpleEchoBot.Models.AwardReasons;

namespace SimpleEchoBot.Models
{
    public class AwardAnalyzer : IAwardAnalyzer
    {
        public AwardAnalyzer()
        {
            var credentials = new TextAnalyticsServiceClientCredentials();
            Client = new TextAnalyticsClient(credentials);
            Client.BaseUri = new Uri(credentials.GetBaseUri());

            Matcher = new GloboforceAwardReasonMatcher();
        }

        public ITextAnalyticsClient Client { get; set; }
        public IAwardReasonMatcher Matcher { get; set; }

        public async Task<string> DetectLanguage(string text)
        {
            BatchInput input = new BatchInput(new List<Input> { new Input { Id = "1", Text = text } });
            try
            {
                var result = Client.DetectLanguageAsync(input).Result;
                if (result == null || result.Documents == null || result.Documents.Count == 0)
                {
                    return null;
                }

                return result.Documents[0].DetectedLanguages.FirstOrDefault()?.Iso6391Name;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<AwardAnalysis> AnalyzeAwardMessageAsync(string text, Dictionary<string, object> context)
        {
            AwardAnalysis result = new AwardAnalysis();
            try
            {
                Task<KeyPhraseBatchResultItem> phrases = GetKeyPhrases(text, context);
                Task<SentimentBatchResultItem> sentiment = GetSentiment(text, context);
                await Task.WhenAll(phrases, sentiment);

                IList<string> phraseStrings = (phrases.Result == null) ? new List<string>() : phrases.Result.KeyPhrases;
                IList<AwardReason> globalReasons = new DummyReasons().GetReasons(phraseStrings);
                Task<IList<AwardReason>> companyReasons = Matcher.MatchAwardReasons(globalReasons, context);
                result.PositiviyScore = CalculatePositivityScore(text, phraseStrings, companyReasons.Result,
                    sentiment.Result, context);
                result.Reasons = companyReasons.Result;
                result.WordCountScore = CalculateWordCountScore(text, context);
                result.KeyPhrases = phraseStrings;
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return result;
            }
        }

        public AwardAnalysis AnalyzeAwardMessage(string text, Dictionary<string, object> context)
        {
            AwardAnalysis result = new AwardAnalysis();
            try
            {
                Task<KeyPhraseBatchResultItem> phrases = GetKeyPhrases(text, context);
                Task<SentimentBatchResultItem> sentiment = GetSentiment(text, context);

                IList<string> phraseStrings = (phrases.Result == null) ? new List<string>() : phrases.Result.KeyPhrases;
                IList<AwardReason> globalReasons = new DummyReasons().GetReasons(phraseStrings);
                Task<IList<AwardReason>> companyReasons = Matcher.MatchAwardReasons(globalReasons, context);
                result.PositiviyScore = CalculatePositivityScore(text, phraseStrings, companyReasons.Result,
                    sentiment.Result, context);
                result.Reasons = companyReasons.Result;
                result.WordCountScore = CalculateWordCountScore(text, context);
                result.KeyPhrases = phraseStrings;
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return result;
            }
        }

        /**
         * <summary>
         * Takes into account different aspects of an award in order to give it
         * a positivity score. Among these aspects are:
         * 
         * Word count of the message.
         * Key phrase count.
         * Highest percentage of the award reason.
         * Sentiment score.
         * 
         * </summary>
         */
        public double CalculatePositivityScore(
            string text,
            IList<string> phraseStrings,
            IList<AwardReason> reasons,
            SentimentBatchResultItem sentiment,
            Dictionary<string, object> context
        )
        {
            double wordCountScore = CalculateWordCountScore(text, context);

            int keyPhraseCount = phraseStrings.Count();
            double keyPhraseScore = Math.Min(keyPhraseCount / (float) GetIdealMessageKeyPhraseCount(context), 1.0);

            float reasonScore = (reasons == null || reasons.Count == 0) ? 0 : reasons[0].Score;
            double sentimentScore = (sentiment == null) ? 0.0 : sentiment.Score.GetValueOrDefault(0);

            /*// Initial implementation uses a simple curve.
            var scores = new List<double>()
            {
                wordCountScore,
                keyPhraseScore,
                reasonScore,
                sentimentScore
            };*/
            return sentimentScore;
        }

        public double CalculateWordCountScore(string text, Dictionary<string, object> context)
        {
            int wordCount = CountWords(text, context);
            int idealWordCount = GetIdealMessageWordCount(context);
            int wordCountDeficit = Math.Max(idealWordCount - wordCount, 0);
            return 1.0 - (wordCountDeficit / (float) idealWordCount);
        }

        int CountWords(string text, Dictionary<string, object> context)
        {
            // TODO: Support something other than Latin alphabet.
            // TODO: Use something smarter than a simple space spitter.
            var punctuation = text.Where(Char.IsPunctuation).Distinct().ToArray();
            var words = text.Split().Select(x => x.Trim(punctuation));
            return words.Count();
        }

        int GetIdealMessageWordCount(Dictionary<string, object> context)
        {
            string lengthString = ConfigurationManager.AppSettings.Get("IdealMessageWordCountDefault");
            return lengthString != null ? Int32.Parse(lengthString) : 15;
        }

        int GetIdealMessageKeyPhraseCount(Dictionary<string, object> context)
        {
            string lengthString = ConfigurationManager.AppSettings.Get("IdealMessageKeyPhraseCountDefault");
            return lengthString != null ? Int32.Parse(lengthString) : 6;
        }

        async Task<KeyPhraseBatchResultItem> GetKeyPhrases(string text, Dictionary<string, object> context)
        {
            MultiLanguageBatchInput input = CreateMultiLanguageInput(text, context);
            HttpOperationResponse<KeyPhraseBatchResult> result = await Client.KeyPhrasesWithHttpMessagesAsync(input);
            if (result == null || result.Body == null || result.Body.Documents == null ||
                result.Body.Documents.Count == 0)
            {
                // TODO: Handle error cases more explicitly instead of failing silently with an empty result.
                return null;
            }

            return result.Body.Documents[0];
        }

        async Task<SentimentBatchResultItem> GetSentiment(string text, Dictionary<string, object> context)
        {
            MultiLanguageBatchInput input = CreateMultiLanguageInput(text, context);
            var result = await Client.SentimentWithHttpMessagesAsync(input);
            if (result == null || result.Body == null || result.Body.Documents == null ||
                result.Body.Documents.Count == 0)
            {
                return null;
            }

            return result.Body.Documents[0];
        }

        MultiLanguageBatchInput CreateMultiLanguageInput(string text, Dictionary<string, object> context)
        {
            string language = GetLanguage(context);
            List<MultiLanguageInput> documents = new List<MultiLanguageInput>()
            {
                new MultiLanguageInput(language, "1", text)
            };
            return new MultiLanguageBatchInput(documents);
        }

        string GetLanguage(Dictionary<string, object> context)
        {
            return context.ContainsKey("language") ? (string) context["language"] : "en";
        }
    }
}