﻿using System.Collections.Generic;

namespace SimpleEchoBot.Models.AwardReasons
{
    public class TeamWork
    {
        public static IList<string> Phrases = new List<string>()
        {
            "harmony",
            "team work",
            "partnership",
            "synergy",
            "unity",
            "alliance",
            "assistance",
            "coalition",
            "confederacy",
            "confederation",
            "federation",
            "help",
            "partisanship",
            "symbiosis",
            "synergism",
            "union",
            "combined effort",
            "doing business with",
            "esprit de corps",
            "joint effort",
            "pulling together",
            "team effort",
            "teaming",
            "working together",
            "support and help",
            "You all jumped",
            "helped when needing",
            "helped when needed",
            "huge help",
            "thanks",
            "team event preparation",
            "thanks guys",
            "thanks girls",
            "thanks all",
            "thank you all",
            "all of you",
            "all together",
            "always helpful",
            "great work",
            "lads",
            "gals",
            "communicator",
            "goals and tasks",
            "enthusiasm and fun",
            "great companions",
            "great colleauges",
            "Yous lift our mood",
            "pleasure to work alongside",
            "pleasure to work",
            "pleasure to work with",
            "excellent work you do",
            "your presence",
            "bridge the gap",
            "teams to understand",
            "under your stewardship",
            "positive attitude",
            "Well done",
            "well prepared",
            "came to play",
            "great feedback",
            "positive impact",
            "unappreciated",
            "keep it up guys",
            "arranged",
            "fun like this",
            "team building",
            "very productive",
            "appreciated",
            "have you participate",
            "benefit to",
            "extra mile",
            "count on you",
            "thought leadership",
            "closer collaboration"
        };
    }
}