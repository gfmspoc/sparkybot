﻿using System.Collections.Generic;
using System.Linq;

namespace SimpleEchoBot.Models.AwardReasons
{
    public class DummyReasons
    {
        public IList<AwardReason> GetReasons(IList<string> keyPhrases)
        {
            Dictionary<string, int> counter = new Dictionary<string, int>();
            var knownReasons = GetReasonsByKeyPhrases();
            foreach (string keyPhrase in keyPhrases)
            {
                if (!knownReasons.ContainsKey(keyPhrase))
                {
                    continue;
                }

                var reason = knownReasons[keyPhrase];
                if (counter.ContainsKey(reason))
                {
                    counter[reason] += 1;
                }
                else
                {
                    counter.Add(reason, 1);
                }
            }

            int totalCount = 0;
            foreach (int count in counter.Values)
            {
                totalCount += count;
            }

            IList<AwardReason> result = new List<AwardReason>();
            foreach (KeyValuePair<string, int> reason in counter)
            {
                result.Add(
                    new AwardReason()
                    {
                        Value = reason.Key,
                        Score = reason.Value / (float) totalCount
                    }
                );
            }

            // Only return the top 3.
            result.OrderByDescending(x => x.Score);
            return result.Take(3).ToList();
        }

        Dictionary<string, string> GetReasonsByKeyPhrases()
        {
            Dictionary<string, string> reasonsByKeyPhrases = new Dictionary<string, string>();
            Dictionary<string, IList<string>> keyPhrasesByReason = GetKeyPhrasesByReason();
            foreach (KeyValuePair<string, IList<string>> reason in keyPhrasesByReason)
            {
                foreach (string keyPhrase in reason.Value)
                {
                    // TODO: Use a Set<string> as the value that is appended if more than one category is encountered.
                    reasonsByKeyPhrases[keyPhrase] = reason.Key;
                }
            }

            return reasonsByKeyPhrases;
        }

        Dictionary<string, IList<string>> GetKeyPhrasesByReason()
        {
            Dictionary<string, IList<string>> result = new Dictionary<string, IList<string>>();
            result.Add("customer service", CustomerService.Phrases);
            result.Add("innovation", Innovation.Phrases);
            result.Add("quality", Quality.Phrases);
            result.Add("team work", TeamWork.Phrases);
            result.Add("urgency", Urgency.Phrases);
            return result;
        }
    }
}