﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SimpleEchoBot.Helpers;

namespace SimpleEchoBot.Models.AwardReasons
{
    /**
     * Concrete implemenation of an IAwardReasonMatcher that gets its data from the Globoforce database (making sure the proper location is used for the company in the context).
     */
    public class GloboforceAwardReasonMatcher : IAwardReasonMatcher
    {
        public async Task<IList<AwardReason>> MatchAwardReasons(IList<AwardReason> reasons,
            Dictionary<string, object> context)
        {
            return reasons;
        }
    }
}