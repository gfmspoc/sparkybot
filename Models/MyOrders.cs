﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using QuickType;
//
//    var welcome = Welcome.FromJson(jsonString);

namespace SimpleEchoBot.Models
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class MyOrders
    {
        [JsonProperty("orders")]
        public Order[] Orders { get; set; }

        [JsonProperty("pageNumber")]
        public long PageNumber { get; set; }

        [JsonProperty("size")]
        public long Size { get; set; }

        [JsonProperty("totalElements")]
        public long TotalElements { get; set; }

        [JsonProperty("totalPages")]
        public long TotalPages { get; set; }

    }

    public partial class Order
    {
        [JsonProperty("createdDate")]
        public long CreatedDate { get; set; }

        [JsonProperty("error")]
        public bool Error { get; set; }

        [JsonProperty("formattedCreatedDate")]
        public string FormattedCreatedDate { get; set; }

        [JsonProperty("orderReference")]
        public string OrderReference { get; set; }

        [JsonProperty("redemptionItems")]
        public RedemptionItem[] RedemptionItems { get; set; }
    }

    public partial class RedemptionItem
    {
        [JsonProperty("bulk")]
        public bool Bulk { get; set; }

        [JsonProperty("cancelledDate")]
        public long CancelledDate { get; set; }

        [JsonProperty("chainCode")]
        public string ChainCode { get; set; }

        [JsonProperty("codesCount")]
        public long CodesCount { get; set; }

        [JsonProperty("destStoreCode")]
        public string DestStoreCode { get; set; }

        [JsonProperty("error")]
        public bool Error { get; set; }

        [JsonProperty("etd")]
        public long Etd { get; set; }

        [JsonProperty("formOfDeliveryDisplayName")]
        public string FormOfDeliveryDisplayName { get; set; }

        [JsonProperty("formOfDeliveryName")]
        public string FormOfDeliveryName { get; set; }

        [JsonProperty("formattedCancelledDate")]
        public string FormattedCancelledDate { get; set; }

        [JsonProperty("formattedEtd")]
        public string FormattedEtd { get; set; }

        [JsonProperty("formattedFxAmount")]
        public string FormattedFxAmount { get; set; }

        [JsonProperty("formattedProcessedDate")]
        public string FormattedProcessedDate { get; set; }

        [JsonProperty("logo")]
        public string Logo { get; set; }

        [JsonProperty("prepaidCardsActivationRequired")]
        public bool PrepaidCardsActivationRequired { get; set; }

        [JsonProperty("processedDate")]
        public long ProcessedDate { get; set; }

        [JsonProperty("productId")]
        public long ProductId { get; set; }

        [JsonProperty("productName")]
        public string ProductName { get; set; }

        [JsonProperty("redemptionItemDenominations")]
        public RedemptionItemDenomination[] RedemptionItemDenominations { get; set; }
    }

    public partial class RedemptionItemDenomination
    {
        [JsonProperty("cancelled")]
        public bool Cancelled { get; set; }

        [JsonProperty("cardProxyId")]
        public string CardProxyId { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("electronicDenominationId")]
        public string ElectronicDenominationId { get; set; }

        [JsonProperty("error")]
        public bool Error { get; set; }

        [JsonProperty("fxCode")]
        public string FxCode { get; set; }

        [JsonProperty("masked")]
        public bool Masked { get; set; }

        [JsonProperty("orderId")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long OrderId { get; set; }

        [JsonProperty("pin")]
        public string Pin { get; set; }

        [JsonProperty("pkPrepaidCard")]
        public string PkPrepaidCard { get; set; }

        [JsonProperty("quantity")]
        public long Quantity { get; set; }

        [JsonProperty("redemptionLink")]
        public string RedemptionLink { get; set; }

        [JsonProperty("securityCode")]
        public string SecurityCode { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("value")]
        public decimal Value { get; set; }

        [JsonProperty("resendChildOrderReference")]
        public string ResendChildOrderReference { get; set; }

        [JsonProperty("resendParentOrderReference")]
        public string ResendParentOrderReference { get; set; }

        [JsonProperty("saleType")]
        public string SaleType { get; set; }

        [JsonProperty("sendAddress1")]
        public string SendAddress1 { get; set; }

        [JsonProperty("sendAddress2")]
        public string SendAddress2 { get; set; }

        [JsonProperty("sendAddress3")]
        public string SendAddress3 { get; set; }

        [JsonProperty("sendCity")]
        public string SendCity { get; set; }

        [JsonProperty("sendCountry")]
        public string SendCountry { get; set; }

        [JsonProperty("sendEmail")]
        public string SendEmail { get; set; }

        [JsonProperty("sendName1")]
        public string SendName1 { get; set; }

        [JsonProperty("sendName2")]
        public string SendName2 { get; set; }

        [JsonProperty("sendStateProv")]
        public string SendStateProv { get; set; }

        [JsonProperty("sendZip")]
        public string SendZip { get; set; }

        [JsonProperty("storeFfCode")]
        public string StoreFfCode { get; set; }

        [JsonProperty("totalAmount")]
        public decimal TotalAmount { get; set; }

        [JsonProperty("totalFx")]
        public string TotalFx { get; set; }

        [JsonProperty("trackMethod")]
        public string TrackMethod { get; set; }

        [JsonProperty("trackParam")]
        public string TrackParam { get; set; }

        [JsonProperty("trackUrl")]
        public string TrackUrl { get; set; }

        [JsonProperty("trackingNumber")]
        public string TrackingNumber { get; set; }
    }

    public partial class Welcome
    {
        public static Welcome FromJson(string json) => JsonConvert.DeserializeObject<Welcome>(json, SimpleEchoBot.Models.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this Welcome self) => JsonConvert.SerializeObject(self, SimpleEchoBot.Models.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class ParseStringConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(long) || t == typeof(long?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            long l;
            if (Int64.TryParse(value, out l))
            {
                return l;
            }
            throw new Exception("Cannot unmarshal type long");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (long)untypedValue;
            serializer.Serialize(writer, value.ToString());
            return;
        }

        public static readonly ParseStringConverter Singleton = new ParseStringConverter();
    }
}
