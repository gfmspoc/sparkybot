﻿using System;

namespace SimpleEchoBot.Models
{
    public class Award
    {
        public string Recipient { get; set; }
        public AwardValue Value { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public AwardVisibility Visibility { get; set; }
        public double Score { get; set; }
    }

    public class AwardValue
    {
        public Decimal Amount { get; set; }
        public string Currency { get; set; }
    }

    public enum AwardVisibility
    {
        Shared,
        Restricted,
        Private
    }
}