﻿namespace SimpleEchoBot
{
    public static class BotEvents
    {
        public const string AwardDialogStarted = "awardDialogStarted";
        public const string PositivityRating = "positivityRating";
        public const string PersonChosen = "personChosen";
        public const string AwardTitle = "awardTitle";
        public const string Amount = "amount";
        public const string Category = "category";
        public const string AwardMessage = "setAwardMessage";
        public const string PlaceNomination = "placeNomination";
        public const string PlaceRedemption = "placeRedemption";
        public const string getMyOrders = "getMyOrders";
    }
}