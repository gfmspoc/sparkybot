﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using SimpleEchoBot.Helpers;
using Newtonsoft.Json;

namespace SimpleEchoBot.Dialogs
{
    [Serializable]
    public class UserAgentDialog : IDialog<object>
    {
        public async Task StartAsync(IDialogContext context)
        {
            var route = RoutingTable.instance();
            bool isFrench = route._user.Language.StartsWith("fr", StringComparison.OrdinalIgnoreCase);
            await context.PostAsync(isFrench ? $"Vous êtes maintenant connecté à: {route._operator._toName} \n\n Tapez 'quitter' pour quitter" : $"You are now connected to: {route._operator._toName} \n\n Type 'quit' to exit");
            context.Wait(MessageReceivedAsync);
        }

        public virtual async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            var message = await activity;
            var routingTable = RoutingTable.instance();
            bool isFrench =  routingTable._user.Language.StartsWith("fr", StringComparison.OrdinalIgnoreCase);

            if (message.Text.StartsWith("quit", StringComparison.OrdinalIgnoreCase) || routingTable._user == null)
            {
                await context.PostAsync(isFrench ? "OK aurevoir" : "Ok.. goodbye");
                if (routingTable._operator != null)
                {
                    routingTable._operator.sendMessage($"User {routingTable._user._toName} left conversation");
                }

                routingTable._user = null;
                context.Done("RETURN_VALUE");
                
                return;
            }

            if (routingTable._operator != null)
            {
                if (routingTable._operator.Language != routingTable._user.Language)
                {
                    var translation =
                        await HttpClientHelper.MakeTranslatorRequest(message.Text, routingTable._user.Language, routingTable._operator.Language);
                    dynamic translationObject = JsonConvert.DeserializeObject(translation);
                    message.Text = translationObject[0]["translations"][0]["text"];
                }
                routingTable._operator.forwardMessage(message, routingTable._user._toName +": ");
                await context.PostAsync(isFrench ? "Envoyé à l'agent" : "Sent to agent");
            }

            context.Wait(MessageReceivedAsync);
        }
    }
}