﻿    using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.PersonalityChat.Core;
using Microsoft.Bot.Connector;
using SimpleEchoBot.Helpers;
using Newtonsoft.Json;
using Microsoft.Bot.Builder.FormFlow;
    using SimpleEchoBot.Forms;

namespace SimpleEchoBot.Dialogs
{
    [Serializable]
    public class RootDialog : IDialog<NominationFormDirectline>
    {
        private readonly BuildFormDelegate<NominationFormDirectline> NewNomination;

        public RootDialog(BuildFormDelegate<NominationFormDirectline> newNomination)
        {
            NewNomination = newNomination;
        }

        public async Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync);
        }

        public virtual async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;

            if (message.Text.StartsWith("/"))
            {
                if (await HandleSystemMessage(context, message))
                    return;
            }
            PersonalityChatOptions PersonalityChatOptions =
                new PersonalityChatOptions(string.Empty, PersonalityChatPersona.Professional);
            PersonalityChatService PersonalityChatService = new PersonalityChatService(PersonalityChatOptions);

            var personalityChatResponse = await PersonalityChatService.QueryServiceAsync(message.Text);
            var qnaResponse = await HttpClientHelper.MakeQNARequest(message.Text);
            var luisResponse = await HttpClientHelper.MakeLUISRequest(message.Text);

            if (luisResponse.TopScoringIntent.Score > 0.7 && luisResponse.TopScoringIntent.Intent != "None")
            {
                switch (luisResponse.TopScoringIntent.Intent)
                {
                    case "NominationStart":
                        {
                            var form = new NominationFormDirectline();
                            if(luisResponse.Entities.Count > 0)
                            {
                                foreach (var entity in luisResponse.Entities)
                                {
                                    switch (entity.Type)
                                    {
                                        case "person":
                                            form.Name = entity.Entity;
                                            break;
                                    }
                                }
                            }
                            var dailog = new FormDialog<NominationFormDirectline>(form, this.NewNomination, FormOptions.PromptInStart);
                            
                            TrackEventHelper.TrackEvent("Nomination", "Start", context.Activity);
                            context.Call(dailog, NominationResultCallback);
                        }
                        break;

                    default:
                        {
                            //context.Wait(this.MessageReceivedAsync);
                            TrackEventHelper.TrackEvent(luisResponse.TopScoringIntent.Intent, "Start", context.Activity);
                            await context.PostAsync("Intent " + luisResponse.TopScoringIntent.Intent);
                        }
                        break;
                }
            }
            else if (personalityChatResponse.IsChatQuery == true)
            {
                await context.PostAsync(personalityChatResponse.ScenarioList[0].Responses[0]);
            }
            else if (qnaResponse.answers[0].answer != "No good match found in KB.")
            {
                await context.PostAsync($"QnA Maker provided this repsonse: {qnaResponse.answers[0].answer}");
                context.Wait(MessageReceivedAsync);
            }
            else
            {
                await context.PostAsync("Sorry I didn't understand that");
                context.Wait(MessageReceivedAsync);
            }
        }

        private async Task NominationResultCallback(IDialogContext context, IAwaitable<object> result)
        {
            var nominationResults = (NominationResults) (await result);

            await context.PostAsync(
                "That's terribly nice of you!! \n\nYour Nomination has been submitted with the following details:\n\n" +
                "Award Recipient: " + nominationResults.Name +
                "\n\nAward Amount: " + nominationResults.Amount +
                "\n\nAward Title: " + nominationResults.Title +
                "\n\nAward Category: " + nominationResults.Category +
                "\n\nVisibility Level: " + nominationResults.AwardAccess +
                "\n\nAward Reason:\n " + nominationResults.Reason);

            context.Wait(MessageReceivedAsync);
            return;
        }

        private async Task<bool> HandleSystemMessage(IDialogContext context, IMessageActivity message)
        {
            string command, text = "";

            string[] sysMsg = message.Text.Split(' ');
            command = sysMsg[0];
            if (sysMsg.Length > 1)
            {
                sysMsg = sysMsg.Skip(1).ToArray();
                text = String.Join(" ", sysMsg);
            }

            switch (command)
            {
                case "/help":
                {
                    TrackEventHelper.TrackEvent("HelpRequest", "Nomination", message);

                    var routingTable = RoutingTable.instance();
                    if (routingTable._operator != null)
                    {
                        if (routingTable._operator._fromId != message.From.Id)
                        {
                            routingTable.setUser(new User(message));
                            string translation =
                                await HttpClientHelper.MakeTranslatorRequest(text, routingTable._user.Language, routingTable._operator.Language);
                            dynamic translationObject = JsonConvert.DeserializeObject(translation);
                            message.Text = translationObject[0]["translations"][0]["text"];
                            routingTable._operator.forwardMessage(message, "USER: ");
                            await context.PostAsync("Sent to operator");
                        }
                    }
                    else
                    {
                        await context.PostAsync("Sorry.. no one to help you");
                    }

                    context.Wait(MessageReceivedAsync);
                    return true;
                }
            }

            return false;
        }

        private async Task OperatorDialogCallback(IDialogContext context, IAwaitable<object> result)
        {
            context.Wait(MessageReceivedAsync);
        }
    }
}