﻿using QnAMakerDialog;
using Microsoft.Bot.Builder.Dialogs;
using System;
using System.Configuration;
using System.Threading.Tasks;
using QnAMakerDialog.Models;
using System.Linq;
using Microsoft.Bot.Builder.PersonalityChat.Core;

namespace SimpleEchoBot.Dialogs
{

    [Serializable]
    public class QnADialog : QnAMakerDialog<object>
    {
        public QnADialog() : base()
        {
            var qnaHostName = ConfigurationManager.AppSettings["QNAHostName"];
            this.BaseUri = $"https://{qnaHostName}/qnamaker";
            this.EndpointKey = ConfigurationManager.AppSettings["QNAAPIKey"];
            this.KnowledgeBaseId = ConfigurationManager.AppSettings["QNAKbId"];
            this.MaxAnswers = 2;
            
        }

        public override async Task DefaultMatchHandler(IDialogContext context, string originalQueryText, QnAMakerResult result)
        {
            string questionAnswered = "";
            if (result.Answers[0].Score == 0)
            {
                PersonalityChatOptions PersonalityChatOptions =
                new PersonalityChatOptions(string.Empty, PersonalityChatPersona.Friendly);
                PersonalityChatService PersonalityChatService = new PersonalityChatService(PersonalityChatOptions);

                var message = $"Sorry, I couldn't find an answer for '{originalQueryText}'.";
                var personalityChatResponse = await PersonalityChatService.QueryServiceAsync(originalQueryText);
                if (personalityChatResponse.IsChatQuery && personalityChatResponse.ScenarioList.Count > 0) {
                    var x = personalityChatResponse.ScenarioList.FirstOrDefault();
                    message = x.Responses.FirstOrDefault();
                }
                await context.PostAsync(message);
            }
            else
            {
                var messageActivity = ProcessResultAndCreateMessageActivity(context, ref result);
                messageActivity.Text = result.Answers.First().Answer;
                questionAnswered = "yes";
                await context.PostAsync(messageActivity);
            }
            context.Done(questionAnswered);
        }

        public override async Task NoMatchHandler(IDialogContext context, string originalQueryText)
        {
            await context.PostAsync($"Sorry, I couldn't find an answer for '{originalQueryText}'.");
            context.Wait(MessageReceived);
        }
    }
}