﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using SimpleEchoBot.Extras;
using SimpleEchoBot.Helpers;
using SimpleEchoBot.Models;
using SimpleEchoBot.BusinessEntities.Employee;

namespace SimpleEchoBot.Dialogs
{
    public class NominationResults
    {
        public string Name { get; set; }
        public string Amount { get; set; }
        public string Title { get; set; }
        public string Reason { get; set; }
        public string AwardAccess { get; set; }
        public string Category { get; set; }
        public IList<AwardReason> AwardReasonClassifications { get; set; }
        private Employee employeeChosenForNomination { get; set; }

    }

    [Serializable]
    public class NominationDialog : IDialog<object>
    {
        private const string EVENT_NAME = "Nomination";
        string Name { get; set; }
        string Amount { get; set; }
        public string Title { get; set; }
        string Reason { get; set; }
        string Category { get; set; }
        public IList<AwardReason> AwardReasonClassifications { get; set; }
        string AwardAccess { get; set; }
        public List<string> AcceptableCategories { get; set; }

        public List<string> AcceptableAmounts { get; set; }

        public async Task StartAsync(IDialogContext context)
        {
            AcceptableCategories =
                new List<string>() {

                    "Innovation = Imagination + Determination",
                    "Respect for Teamwork",
                    "Respect for Customers",
                    "Respect for Urgency",
                    "Respect for Quality",
                    "Respect for All",
                    "Respect for my authoritae!"
                };

            AcceptableAmounts = new List<string> { "25", "50", "100", "200", "500" };

            await context.SendEventToBot(BotEvents.AwardDialogStarted, "dialog.started");

            await context.PostAsync("Who would you like to nominate?");
            TrackEventHelper.TrackEvent(EVENT_NAME, "Who", context.Activity);
            context.Wait(MessageReceivedAsync);
        }

        public virtual async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            if (string.IsNullOrWhiteSpace(message.Text))
            {
                await context.PostAsync("Unfortunately, the name is not provided");
            }

            Name = message.Text;

            //context.SendEventToBot(, message.Text);
            // TODO: replace this with going out to the company "graph" and getting the 
            // actual user ID
            var userId = Name.ToLowerInvariant().Replace(' ', '.');

            await context.SendEventToBot(BotEvents.PersonChosen, userId);

            await context.PostAsync(CreateAmountCard(context));
            TrackEventHelper.TrackEvent(EVENT_NAME, "HowMuch", context.Activity);

            context.Wait(AmountReceivedAsync);
        }


        public virtual async Task EmployeeNameReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            if (!AcceptableAmounts.Contains(message.Text))
            {
                await context.PostAsync("Please choose a name from the below:");
                await context.PostAsync(CreateAmountCard(context));
                context.Wait(EmployeeNameReceivedAsync);
                return;
            }
            Amount = message.Text;
            await context.SendEventToBot(BotEvents.Amount, Amount);

            var employeeNameRequestString = "What's the Employee Name?";

            await context.PostAsync(employeeNameRequestString);
            TrackEventHelper.TrackEvent(EVENT_NAME, "EmployeeName", context.Activity);

            context.Wait(ReasonReceivedAsync);
        }



        public virtual async Task AmountReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            if (!AcceptableAmounts.Contains(message.Text))
            {
                await context.PostAsync("Please choose a valid Amount from below:");
                await context.PostAsync(CreateAmountCard(context));
                context.Wait(AmountReceivedAsync);
                return;
            }
            Amount = message.Text;
            await context.SendEventToBot(BotEvents.Amount, Amount);

            var reasonRequestString = "What's the reason for giving " + Name + " this award?";

            await context.PostAsync(reasonRequestString);
            TrackEventHelper.TrackEvent(EVENT_NAME, "Reason", context.Activity);

            context.Wait(ReasonReceivedAsync);
        }

        public virtual async Task ReasonReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            Reason = message.Text;

            await context.SendEventToBot(BotEvents.AwardMessage, message.Text);

            var analyzer = new AwardAnalyzer();
            var analysis = await analyzer.AnalyzeAwardMessageAsync(Reason, new Dictionary<string, object>());
            AwardReasonClassifications = analysis.Reasons;

            var posScore = Math.Round(analysis.PositiviyScore * 100);
            await context.SendEventToBot(BotEvents.PositivityRating, posScore.ToString());

            if (analysis.WordCountScore < 0.99)
            {
                await context.PostAsync(
                    "You might like to know that your award message is below the average. Please could you enter a reason that is at least 15 words long.");
                context.Wait(ReasonReceivedAsync);
                return;
            }

            if (analysis.KeyPhrases.Count < 3)
            {
                await context.PostAsync(
                    "I'm afraid there aren't enough key phrases in your award reason. Please could you enter a richer explanation of why you'd like to give " +
                    Name + " this award.");
                context.Wait(ReasonReceivedAsync);
                return;
            }

            var titleRequestString = "What Title will we give this award?";
            await context.PostAsync(titleRequestString);
            TrackEventHelper.TrackEvent(EVENT_NAME, "Title", context.Activity);

            context.Wait(TitleReceivedAsync);
        }

        public virtual async Task TitleReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            Title = message.Text;
            await context.SendEventToBot(BotEvents.AwardTitle, Title);


            await context.PostAsync(CreateCategoryCard(context));
            TrackEventHelper.TrackEvent(EVENT_NAME, "Category", context.Activity);

            context.Wait(CategoryReceivedAsync);
        }

        public virtual async Task CategoryReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            if (!AcceptableCategories.Contains(message.Text))
            {
                await context.PostAsync("Please choose a valid category from below:");
                await context.PostAsync(CreateCategoryCard(context));
                context.Wait(CategoryReceivedAsync);
                return;
            }

            Category = message.Text;

            await context.SendEventToBot(BotEvents.Category,
                (Category ?? string.Empty).ToLowerInvariant().Replace(" ", ""));

            await context.PostAsync(CreateMessageAccessCard(context));
            TrackEventHelper.TrackEvent(EVENT_NAME, "Access", context.Activity);
            context.Wait(AwardMessageAccessAsync);
        }

        public virtual async Task AwardMessageAccessAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            AwardAccess = message.Text;

            await context.SendEventToBot("setPrivacyLevel", message.Text);
            TrackEventHelper.TrackEvent(EVENT_NAME, "Complete", context.Activity);

            context.Done(new NominationResults()
            {
                Amount = Amount,
                Name = Name,
                Title = Title,
                Reason = Reason,
                Category = Category,
                AwardAccess = AwardAccess
            });
        }

        private IMessageActivity CreateCategoryCard(IDialogContext context)
        {
            var reply = context.MakeMessage();

            reply.AttachmentLayout = AttachmentLayoutTypes.List;
            reply.Attachments = new List<Attachment>();

            List<CardAction> cardButtons = new List<CardAction>();
            foreach (var category in AcceptableCategories)
            {
                cardButtons.Add(new CardAction()
                {
                    Value = category,
                    Title = category,
                    Type = "imBack"
                });
            }
           
            string subtitle = string.Empty;
            if (AwardReasonClassifications.Count > 0)
            {
                subtitle = "Based on your award message, I think it should be: ";
                for (int i = 0; i < AwardReasonClassifications.Count - 1; i++)
                    subtitle = subtitle + AwardReasonClassifications[i].Value + " or ";
                subtitle = subtitle + AwardReasonClassifications[AwardReasonClassifications.Count - 1].Value;
            }

            HeroCard heroCard = new HeroCard()
            {
                Title = "Please choose the category you'd like to assign this award to.",
                Subtitle = subtitle,
                Buttons = cardButtons
            };

            Attachment attachment = heroCard.ToAttachment();
            reply.Attachments.Add(attachment);

            return reply;
        }

        private IMessageActivity CreateAmountCard(IDialogContext context)
        {
            var reply = context.MakeMessage();

            reply.AttachmentLayout = AttachmentLayoutTypes.List;
            reply.Attachments = new List<Attachment>();

            List<CardAction> cardButtons = new List<CardAction>();

            foreach (var amount in AcceptableAmounts)
            {
                cardButtons.Add(new CardAction() { Value = amount, Title = amount, Type = "imBack" });
            }

            HeroCard heroCard = new HeroCard()
            {
                Title = "Please choose the amount you would like to award.",
                Buttons = cardButtons
            };

            Attachment attachment = heroCard.ToAttachment();
            reply.Attachments.Add(attachment);

            return reply;
        }

        private IMessageActivity SelectPotentialEmp (IDialogContext context, List<string> potentialEmp)
        {
            var reply = context.MakeMessage();
            reply.AttachmentLayout = AttachmentLayoutTypes.List;
            reply.Attachments = new List<Attachment>();

            List<CardAction> cardButtons = new List<CardAction>();


            foreach (var p in potentialEmp)
            {
                cardButtons.Add(new CardAction() { Value = p, Type = "imBack" });
            }

            return reply;
        }


        private IMessageActivity CreateMessageAccessCard(IDialogContext context)
        {
            var reply = context.MakeMessage();

            reply.AttachmentLayout = AttachmentLayoutTypes.List;
            reply.Attachments = new List<Attachment>();

            List<CardAction> cardButtons = new List<CardAction>();
            CardAction sharedButton = new CardAction()
            {
                Value = "Shared",
                Title = "Shared",
                Type = "imBack"
            };

            CardAction restrictedButton = new CardAction()
            {
                Value = "Restricted",
                Title = "Restricted",
                Type = "imBack"
            };
            CardAction privateButton = new CardAction()
            {
                Value = "Private",
                Title = "Private",
                Type = "imBack"
            };
            cardButtons.Add(sharedButton);
            cardButtons.Add(restrictedButton);
            cardButtons.Add(privateButton);

            HeroCard heroCard = new HeroCard()
            {
                Title = "Please choose the message access level",
                Buttons = cardButtons
            };




            Attachment attachment = heroCard.ToAttachment();
            reply.Attachments.Add(attachment);

            return reply;
        }
    }
}