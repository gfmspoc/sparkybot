﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;
using Microsoft.Bot.Connector;
using SimpleEchoBot.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using SimpleEchoBot.Forms;
using SimpleEchoBot.Extras;
using AdaptiveCards;
using SimpleEchoBot.Models;
using System.Linq;
using SimpleEchoBot.Exceptions;

namespace SimpleEchoBot.Dialogs
{
    [Serializable]
    public class LUISDialog : LuisDialog<object>
    {
        public LUISDialog() : base(new LuisService(new LuisModelAttribute(
           ConfigurationManager.AppSettings["LUISAppId"],
           ConfigurationManager.AppSettings["LUISAPIKey"],
           LuisApiVersion.V2,
           ConfigurationManager.AppSettings["LuisAPIHostName"],
           0.7)))
        {
        }

        /*public async Task None(IDialogContext context, IAwaitable<IMessageActivity> activity, LuisResult result)
        {
            var msg = await activity;
            TextAnalyticsService analyticsService = new TextAnalyticsService();
            var sentiment = await analyticsService.GetSentiment(msg.Text, msg.Locale);
            if (sentiment != null && sentiment.Score < 0.28)
            {
                
                //PromptDialog.Confirm(context, ResumeAfterPromptForAgent, new PromptOptions<string>("Would you like to speak to an Agent?"));
                PromptDialog.Confirm(context, ResumeAfterPromptForAgent, $"Would you like to speak to an Agent ?");
             }
            else
            {
                await context.Forward(new QnADialog(), Callback, msg, context.CancellationToken);
            }
        }*/

        #region OrderInformation
        [LuisIntent("OrderLocation")]
        public async Task OrderInformation(IDialogContext context, IAwaitable<IMessageActivity> activity, LuisResult result)
        {


            //commented out for demo
          //   var form = new OrderSearchForm();
          //   var dialog = new FormDialog<OrderSearchForm>(form, OrderSearchForm.BuildForm, FormOptions.PromptInStart);


        //  context.Call(dialog, OrderSearchFormComplete);
           


          await demoCode(context);
            

        }

        private async Task demoCode(IDialogContext context)
        {

            if (context.Activity.ChannelId == ChannelIds.Directline)
            {
                //  await context.SendEventToBot(BotEvents.getMyOrders, orderSearch.ToJson());
                await context.SendEventToBot(BotEvents.getMyOrders, "someValue");
            }
            else if(context.Activity.ChannelId == ChannelIds.Sms)
            {
                var list = GetRedemptionItems();
                await context.PostAsync("You can find your last 3 order(s) below:");

                string message = "";

                foreach (var item in list)
                {
                    message = $"Product Name: {item.ProductName}, ";
                    message = string.Concat(message, $"Order value: {item.FormattedFxAmount}, ");
                    message = string.Concat(message, $"Estimated delivery by: {item.FormattedProcessedDate}, ");
                    message = string.Concat(message, $"Form of delivery: {item.FormOfDeliveryName}");

                    await context.PostAsync(message);
                }
            }
            else
            {
                var list = new List<RedemptionItem>()
                    {
                       /* new RedemptionItem
                        {
                            FormattedEtd = "giftcard",
                            ProductName = "Easons",
                            Logo = "../img/logos/IRLBVCEASO.gif",
                            FormattedFxAmount = "EUR 18.00",
                            ChainCode = "",
                            FormOfDeliveryName = "Post"
                        },*/
                        new RedemptionItem
                        {
                            FormattedEtd = "merchandise",
                            ProductName = "Sun Glasses",
                            Logo = "../img/logos/F03_20_3_p.JPG",
                            FormattedFxAmount = "EUR 50:00",
                            ChainCode = "",
                            FormOfDeliveryName = "Post"
                        } /*,
                        new RedemptionItem
                        {
                            FormattedEtd = "ecert",
                            ProductName = "GAP eCert",
                            Logo = "../img/logos/CANSHOGAPI.gif",
                            FormattedFxAmount = "EUR 25:00",
                            ChainCode = "WWW",
                            FormOfDeliveryName = "e-mail"
                        }*/
                    };
                var message = context.Activity as Activity;
                Activity reply = message.CreateReply();

                reply.Attachments = new List<Attachment>();
                var sod = new SimpleOrderData();
                sod.FormattedCreatedDate = "27th August 2018"; //orderSearch.OrderDate.ToString("dddd, dd MMMM yyyy");
                sod.OrderReference = "XX-7QUHN-9VKSB-C7QDU-V8ZXX";
                sod.SaleType = "merchandise";
                sod.FormOfDelivery = "merchandise";

                foreach (RedemptionItem r in list.Where(x => x.FormattedEtd.Equals("merchandise", StringComparison.OrdinalIgnoreCase)))
                {
                    var card = AdaptiveCardsHelper.CreateOrderDetailsAdaptiveCard(message, r, sod);

                    reply.Attachments.Add(new Attachment()
                    {
                        ContentType = AdaptiveCard.ContentType,
                        Content = card
                    });
                }

                await context.PostAsync(reply);


            
                context.Wait(MessageReceived);


                await context.PostAsync(CreatePersistenceCard(context));
                
            }
        }

        private async Task OrderSearchFormComplete(IDialogContext context, IAwaitable<OrderSearchForm> result)
        {
            OrderSearchForm orderSearch = null;

            try
            {
                orderSearch = await result;
              

                //  double unixSeconds = UnixTimeConvert.ConvertToUnixTimestamp(orderSearch.OrderDate);
                



                if (context.Activity.ChannelId == ChannelIds.Directline)
                {

                    //  await context.SendEventToBot(BotEvents.getMyOrders, orderSearch.ToJson());
                    await context.SendEventToBot(BotEvents.getMyOrders, "someValue");
                    PromptDialog.Confirm(context, ResumeAfterQuestion, "Is there anything else we can help you with today?");


                }
                else
                {
                    var list = new List<RedemptionItem>()
                    {
                        new RedemptionItem
                        {
                            FormattedEtd = "giftcard",
                            ProductName = "Easons",
                            Logo = "../img/logos/IRLBVCEASO.gif",
                            FormattedFxAmount = "EUR 18.00",
                            ChainCode = "",
                            FormOfDeliveryName = "Post"
                        },
                        new RedemptionItem
                        {
                            FormattedEtd = "merchandise",
                            ProductName = "Sun Glasses",
                            Logo = "../img/logos/F03_20_3_p.JPG",
                            FormattedFxAmount = "EUR 50:00",
                            ChainCode = "",
                            FormOfDeliveryName = "Courier"
                        },
                        new RedemptionItem
                        {
                            FormattedEtd = "ecert",
                            ProductName = "GAP eCert",
                            Logo = "../img/logos/CANSHOGAPI.gif",
                            FormattedFxAmount = "EUR 25:00",
                            ChainCode = "WWW",
                            FormOfDeliveryName = "e-mail"
                        }
                    };
                    var message = context.Activity as Activity;
                    Activity reply = message.CreateReply();

                    reply.Attachments = new List<Attachment>();
                    var sod = new SimpleOrderData();
                   // sod.FormattedCreatedDate = orderSearch.OrderDate.ToString("dddd, dd MMMM yyyy");// "27th August 2018";
                    sod.OrderReference = "XX-7QUHN-9VKSB-C7QDU-V8ZXX";
                   // sod.SaleType = orderSearch.OrderType.ToString();
                    sod.FormOfDelivery = "ecert";

                    // foreach (RedemptionItem r in list.Where(x => x.FormattedEtd.Equals(orderSearch.OrderType.ToString(), StringComparison.OrdinalIgnoreCase)))
                    foreach (RedemptionItem r in list)
                    {
                        var card = AdaptiveCardsHelper.CreateOrderDetailsAdaptiveCard(message, r, sod);

                        reply.Attachments.Add(new Attachment()
                        {
                            ContentType = AdaptiveCard.ContentType,
                            Content = card
                        });
                    }

                    await context.PostAsync(reply);


                     PromptDialog.Confirm(context, ResumeAfterQuestion, "Now that I have helped you find your order - Is there anything else we can help you with today?");

                }
            }
            catch (OperationCanceledException ex)
            {

                if (ex.InnerException is FormFlowExitException)
                {
                    var ordersaddress = "https://test-web1-21.corp.globoforce.com/microsites/t/dashboard/MyOrders?client=staff";
                    var feedbackForEcert = "";
                    feedbackForEcert = String.Format("You can see the details of you e-cert orders here \n : {0}  ", ordersaddress);
                    await context.PostAsync(feedbackForEcert);
                    await context.PostAsync(CreatePersistenceCard(context));
                    return;
                }


                await context.PostAsync("You canceled the form!");
                return;
            }


            if (orderSearch == null)
            {
                await context.PostAsync("Form returned empty response!");
            }


            context.Wait(MessageReceived);
        }

        #endregion


        #region EscalationToAgent Flow
        [LuisIntent("Escalate")]
        public async Task EscalationToAgent(IDialogContext context, IAwaitable<IMessageActivity> activity, LuisResult result)
        {
            var message = await activity;
            string prompt = message.Locale.StartsWith("fr", StringComparison.OrdinalIgnoreCase) ? "Voulez-vous parler à un Agent?" : "Would you like to speak to an Agent?";
            PromptDialog.Confirm(context, ResumeAfterPromptForAgent, prompt);
        }
        private async Task ResumeAfterPromptForAgent(IDialogContext context, IAwaitable<bool> result)
        {
            var callAgent = await result;
            var message = context.Activity.AsMessageActivity();
            bool isFrench = message.Locale.StartsWith("fr", StringComparison.OrdinalIgnoreCase);
            if (callAgent)
            {
                var routingTable = RoutingTable.instance();
                if (routingTable._operator == null)
                {

                    string prompt = isFrench ? "Malheureusement tous nos agents sont occupés, veuillez réessayer plus tard" : "Unfortunately all our agents are busy, please try again later";
                    await context.PostAsync(prompt);
                    context.Wait(MessageReceived);
                }
                else
                {
                    var user = new User(message);
                    routingTable.setUser(user);
                    message.Text = $"User: {user._toName}, is requesting help";
                    routingTable._operator.forwardMessage(message, "");
                    context.Call(new UserAgentDialog(), Callback);
                }
            }
            else
            {
                string prompt = isFrench ? "Pas de problème, je suis là pour vous aider." : "No problem, I am here to help.";
                await context.PostAsync(prompt);
            }
        }
        #endregion

        #region Exit Flow
        [LuisIntent("Exit")]
        public async Task ExitFlow(IDialogContext context, IAwaitable<IMessageActivity> activity, LuisResult result)
        {
            await SayGoodbye(context);
        }

        private static async Task SayGoodbye(IDialogContext context)
        {
            await context.PostAsync("It was great to talk to you! Good bye.");
            context.Done("");
        }
        #endregion

        #region QandAmaker Flow
        [LuisIntent("None")]
        [LuisIntent("Personality")]
        public async Task Personality(IDialogContext context, IAwaitable<IMessageActivity> activity, LuisResult result)
        {
            var msg = await activity;

            await context.Forward(new QnADialog(), Callback, msg, context.CancellationToken);
        }

        [LuisIntent("QandA")]
        public async Task QandA(IDialogContext context, IAwaitable<IMessageActivity> activity, LuisResult result)
        {
            var msg = await activity;

            await context.Forward(new QnADialog(), QnACallback, msg, context.CancellationToken);
        }

        private async Task QnACallback(IDialogContext context, IAwaitable<object> result)
        {
            var response = await result as string;
            if (string.IsNullOrWhiteSpace(response))
            {
                context.Wait(MessageReceived);
            } else
            {
                PromptDialog.Confirm(context, ResumeAfterQuestion, "Is there anything else we can help you with today?");
            }
        }


        private async Task ResumeAfterQuestion(IDialogContext context, IAwaitable<bool> result)
        {
            var moreHelp = await result;
            if (moreHelp)
            {
                var message = context.Activity as Activity;
                Activity reply = message.CreateReply();
                string firstName = GetFirstName(message.From.Name);
                reply.Attachments = new List<Attachment>();
                var card = AdaptiveCardsHelper.PersistenceAdaptiveCard(firstName, message.ChannelId);

                reply.Attachments.Add(new Attachment()
                {
                    ContentType = AdaptiveCard.ContentType,
                    Content = card
                });

                await context.PostAsync(reply);
            }
            else
            {
                await SayGoodbye(context);
            }
        }
        //Generic callback to keep the conversation going
        private async Task Callback(IDialogContext context, IAwaitable<object> result)
        {
            context.Wait(MessageReceived);
        }
        #endregion

        #region NominationStart Flow
        [LuisIntent("NominationStart")]
        public async Task Nominate(IDialogContext context, LuisResult result)
        {
            var entities = new List<EntityRecommendation>(result.Entities);
            if (result.Entities.Count > 0)
            {
                foreach (var entity in entities)
                {
                    switch (entity.Type)
                    {
                        case "person":
                            entity.Type = "Name";
                            break;
                        case "number":
                            entity.Type = "amount";
                            break;
                    }
                }
            }

            TrackEventHelper.TrackEvent("Nomination", "Start", context.Activity);

            if (context.Activity.ChannelId == ChannelIds.Directline)
            {
                var form = new NominationFormDirectline();
                var dialog = new FormDialog<NominationFormDirectline>(form, NominationFormDirectline.BuildForm, FormOptions.PromptInStart, entities);
                context.Call(dialog, NominationFormCompleteDL);
            }
            else
            {
                var form = new NominationForm();
                var dialog = new FormDialog<NominationForm>(form, NominationForm.BuildForm, FormOptions.PromptInStart, entities);
                context.Call(dialog, NominationFormComplete);
            }
        }

        private async Task NominationFormCompleteDL(IDialogContext context, IAwaitable<NominationFormDirectline> result)
        {
            NominationFormDirectline nomination = null;
            try
            {
                nomination = await result;
                await context.SendEventToBot(BotEvents.PlaceNomination, nomination.ToJson());
            }
            catch (OperationCanceledException)
            {
                await context.PostAsync("You canceled the form!");
                return;
            }

            if (nomination == null)
            {
                await context.PostAsync("Form returned empty response!");
            }

            await context.PostAsync(CreatePersistenceCard(context));

            context.Wait(MessageReceived);
        }

        private async Task NominationFormComplete(IDialogContext context, IAwaitable<NominationForm> result)
        {
            NominationForm nomination = null;
            try
            {
                nomination = await result;
                await context.SendEventToBot(BotEvents.PlaceNomination, nomination.ToJson());
            }
            catch (OperationCanceledException ex)
            {
                await context.PostAsync("You canceled the form!");

                if (ex.InnerException is ArgumentException)
                {
                    PromptDialog.Confirm(context, ResumeAfterPromptForAgent, $"Would you like to speak to an Agent ?");
                    return;
                }
            }

            if (nomination == null)
            {
                await context.PostAsync("Form returned empty response!");
            }

            await context.PostAsync(CreatePersistenceCard(context));

            context.Wait(MessageReceived);
        }
        #endregion

        #region Redemption flow

        [LuisIntent("Redemption")]
        public async Task Redeem(IDialogContext context, LuisResult result)
        {
            if(context.Activity.ChannelId == ChannelIds.Directline)
            {
                var dialog = new FormDialog<RedemptionFormDirectline>(new RedemptionFormDirectline(), RedemptionFormDirectline.BuildForm, FormOptions.PromptInStart);
                context.Call(dialog, RedemptionFormDirectLineComplete);
            } else
            {
                var dialog = new FormDialog<RedemptionForm>(new RedemptionForm(), RedemptionForm.BuildForm, FormOptions.PromptInStart);
                context.Call(dialog, RedemptionFormComplete);
            }
        }

        private async Task RedemptionFormDirectLineComplete(IDialogContext context, IAwaitable<RedemptionFormDirectline> result)
        {
            RedemptionFormDirectline redemption = null;
            try
            {
                redemption = await result;
            }
            catch (OperationCanceledException ex)
            {
                await context.PostAsync("You canceled the form!");
                return;
            }

            if (redemption == null)
            {
                await context.PostAsync("Form returned empty response!");
            }

            await context.PostAsync(CreatePersistenceCard(context));

            context.Wait(MessageReceived);

        }

        private async Task RedemptionFormComplete(IDialogContext context, IAwaitable<RedemptionForm> result)
        {
            RedemptionForm redemption = null;
            try
            {
                redemption = await result;
            }   
            catch (OperationCanceledException ex)
            {
                await context.PostAsync("You canceled the form!");
                return;
            }

            if (redemption == null)
            {
                await context.PostAsync("Form returned empty response!");
            }
            if (context.Activity.ChannelId != ChannelIds.Sms)
            {
                await context.PostAsync(CreatePersistenceCard(context));
            }
            context.Wait(MessageReceived);

        }

        #endregion

        #region Helpers - AdaptiveCards
        private static IMessageActivity CreatePersistenceCard(IDialogContext context)
        {
            var reply = context.MakeMessage();
            var firstName = GetFirstName(context.Activity.From.Name);
            reply.Attachments = new List<Attachment>();

            var card = AdaptiveCardsHelper.PersistenceAdaptiveCard(firstName, context.Activity.ChannelId);

            reply.Attachments.Add(new Attachment()
            {
                ContentType = AdaptiveCard.ContentType,
                Content = card
            });

            return reply;
        }

        private static string GetFirstName(string name)
        {
            if (string.IsNullOrWhiteSpace(name)) return "Globo Buddy";

            if (name.StartsWith(" "))
            {
                name = name.Remove(0, 1);
            }

            return name.Split(' ')[0];
        }

        private List<RedemptionItem> GetRedemptionItems()
        {
            var list = new List<RedemptionItem>()
                    {
                        new RedemptionItem
                        {
                            FormattedEtd = "giftcard",
                            ProductName = "Easons",
                            FormattedFxAmount = "EUR 18.00",
                            FormOfDeliveryName = "Post",
                            FormattedProcessedDate = "04 Sep 2018"
                        },
                        new RedemptionItem
                        {
                            FormattedEtd = "merchandise",
                            ProductName = "Sun Glasses",
                            FormattedFxAmount = "EUR 50:00",
                            FormOfDeliveryName = "Fedex",
                            FormattedProcessedDate = "15 Sep 2018"
                        },
                        new RedemptionItem
                        {
                            FormattedEtd = "ecert",
                            ProductName = "GAP eCert",
                            FormattedFxAmount = "EUR 25:00",
                            FormOfDeliveryName = "e-mail",
                            FormattedProcessedDate = "01 Sep 2018",
                            
                        }
                    };
            return list;
        }
        #endregion
    }

}