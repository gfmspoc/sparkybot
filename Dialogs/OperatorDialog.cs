﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using SimpleEchoBot.Helpers;
using Newtonsoft.Json;

namespace SimpleEchoBot.Dialogs
{
    [Serializable]
    public class OperatorDialog : IDialog<object>
    {
        public async Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync);
        }

        public virtual async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            var message = await activity;
            var routingTable = RoutingTable.instance();

            if (message.Text == "quit" || routingTable._operator == null)
            {
                await context.PostAsync("Ok.. no longer an operator");

                if (routingTable._user != null)
                {
                    routingTable._user.sendMessage($"Operator {routingTable._operator._toName} left conversation");
                }
                routingTable._operator = null;
                context.Done("RETURN_VALUE");
                return;
            }

            if (routingTable._user != null)
            {
                if (routingTable._operator.Language != routingTable._user.Language)
                {
                    var translation =
                        await HttpClientHelper.MakeTranslatorRequest(message.Text, routingTable._operator.Language, routingTable._user.Language);
                    dynamic translationObject = JsonConvert.DeserializeObject(translation);
                    message.Text = translationObject[0]["translations"][0]["text"];
                }
                routingTable._user.forwardMessage(message, routingTable._operator._toName + ": ");
                await context.PostAsync("Sent to user");
            }

            context.Wait(MessageReceivedAsync);
        }
    }
}