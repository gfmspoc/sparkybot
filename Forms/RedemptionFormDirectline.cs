﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using SimpleEchoBot.BusinessEntities.Employee;
using SimpleEchoBot.Models;
using Newtonsoft.Json;
using Microsoft.Bot.Builder.FormFlow.Advanced;
using SimpleEchoBot.Helpers;

namespace SimpleEchoBot.Forms
{
    public enum GiftCardCategories
    {
        [Terms(new[] { "All"})]
        All = 1,
        [Terms(new[] { "Electronics" })]
        Electronics,
        [Terms(new[] { "Fashion" })]
        Fashion,
        [Terms(new[] { "House && Home" })]
        HouseHome,
        [Terms(new[] { "Sports && Recreation" })]
        SportsRecreation,
        [Terms(new[] { "Travel" })]
        Travel,
        [Terms(new[] { "Health && Beauty" })]
        HealthBeauty,
        [Terms(new[] { "Children" })]
        Children,
        [Terms(new[] { "Charity" })]
        Charity
    }

    public enum GiftCardDenomination
    {
        [Terms(new[] { "25", "Twenty Five" })]
        TwentyFive = 25,
        [Terms(new[] { "50", "Fifty" })]
        Fifty = 50,
        [Terms(new[] { "100", "One Hundred" })]
        OneHundred = 100
    }

    public enum Awards
    {
        [Terms(new[] { "100", "One Hundred" })]
        OneHundred = 100,
        [Terms(new[] { "200", "Two Hundred" })]
        TwoHundred = 200,
        [Terms(new[] { "500", "Five Hundred" })]
        FiveHundred = 500
    }

    [Serializable]
    public class RedemptionFormDirectline
    {


        [Prompt("Please select giftcard category. {||}")]
        public GiftCardCategories GiftcardCategory { get; set; }

        [Prompt("Please enter at least 3 letters from GiftCard name you want to redeem for.")]
        public string GiftcardName { get; set; }

        [Prompt("Please select gift card denomination. {||}")]
        public GiftCardDenomination Denomination { get; set; }

        [Prompt("Please enter quantity.")]
        public int Quantity { get; set; }

        [Prompt("Please enter delivery name.")]
        public string DeliveryName { get; set; }
      
        [Prompt("Please enter delivery address.")]
        public string Address1 { get; set; }

        [Prompt("Please enter the email address.")]
        public string EmailAddress{ get; set; }

        [Prompt("Please select an {&} you want to use to pay for your order. {||}")]
        public Awards Award { get; set; }

        private GiftCard giftCardSelected { get; set; }
        private string LastMessage { get; set; }

        public static IForm<RedemptionFormDirectline> BuildForm()
        {
            return new FormBuilder<RedemptionFormDirectline>()
                .Field(nameof(GiftcardCategory))
                .Field(nameof(GiftcardName), validate: async (state, value) =>
                    {
                        var nameToCheck = (string)value;

                        var results = new ValidateResult { IsValid = true, Value = value };
                        if (nameToCheck.Length >= 3)
                        {
                            var data = DataLookupHelper.Instance();
                            var giftCards = data.FindGiftCards(nameToCheck);

                            if (giftCards.Count > 1)
                            {
                                string feedback = "I found a few gift cards that contain name you entered, did you mean any of the following?\n Type help to list available options.";

                                IEnumerable<Choice> choices = giftCards.Select((e, i) => new Choice()
                                {
                                    Description = new DescribeAttribute(e.ToString(), null, null, null, null),
                                    Terms = new TermsAttribute() { Alternatives = new[] { e.ToString(), (i + 1).ToString() } },
                                    Value = e.ToString()
                                });

                                results.Choices = choices;
                                results.Feedback = feedback;
                                results.IsValid = false;
                            }
                            else if (giftCards.Count == 0)
                            {
                                results.Feedback = $"It seems that we are not offering any gift cards with that name .... ";
                                results.IsValid = false;
                            }
                            else
                            {
                                state.giftCardSelected = giftCards[0];
                                results.Value = state.giftCardSelected.ToString();
                            }

                        }
                        else
                        {
                            results.Feedback = $"Unfortunately, there is not enough characters entered. You need to enter at least 3 characters. \nPlease enter again.";
                            results.IsValid = false;
                        }
                        return results;
                    })
                .Prompter(async (context, prompt, state, field) =>
                    {
                        var preamble = context.MakeMessage();
                        var promptMessage = context.MakeMessage();

                        if (prompt.GenerateMessages(preamble, promptMessage))
                        {
                            await context.PostAsync(preamble);
                        }

                        if (state.LastMessage != "I found a few gift cards that contain name you entered, did you mean any of the following?\n Type help to list available options.")
                        {
                            await context.PostAsync(promptMessage);
                        }

                        state.LastMessage = promptMessage.Text;

                        return prompt;
                    }
                 ).Message("You selected: {GiftcardName}")                            
                .Field(nameof(Denomination))
                .Field(nameof(Quantity))
                    .Confirm(async (state) =>
                    {
                        var summary = (int)state.Denomination * state.Quantity;
                        return new PromptAttribute($"Total for your order is {summary:C2}. Is that OK?");
                    })
                .Field(nameof(DeliveryName))
                .Field(nameof(Address1))
                .Field(nameof(EmailAddress))
                .Field(nameof(Award))
                .AddRemainingFields()
                .Confirm("Does this look good to you?\n {*} {||}")
                .OnCompletion(Callback)
                .Build();

        }

        /*public string ToJson()
        {
            return JsonConvert.SerializeObject(ToDto());
        }

        public NominationToPlace ToDto()
        {
            var data = DataLookupHelper.Instance();
            NominationToPlace n = new NominationToPlace()
            {
                reason = data.GetAwardReasonId(Category.ToString()),
                title = Title,
                messageToRecipient = Reason,
               // messageToApprover = $"Please approve this award, {Title}",
                privacy = AwardAccess.ToString(),
                language = "eng"
                
            };

            n.nomineeAwards.Add(new NomineeAwards { nominee = employeeChosenForNomination.ID, awardType = data.GetAwardTypeId( Amount.ToString()) });

            return n;
        }*/
        private static Task Callback(IDialogContext context, RedemptionFormDirectline state)
        {
            return context.PostAsync($"Thanks for placing the order. We will confirm when your order gets shipped.");
        }
    }
}
