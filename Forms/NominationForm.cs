﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Builder.FormFlow.Advanced;
using Newtonsoft.Json;
using SimpleEchoBot.BusinessEntities.Employee;
using SimpleEchoBot.Models;

namespace SimpleEchoBot.Forms
{
    public enum AwardAccessOptions
    {
        Shared = 1,
        Restricted,
        Private
    }

    public enum AwardCategories
    {
        RespectForAll = 1,
        Innovation,
        RespectForQuality,
        RespectForTeamwork,
        RespectForUrgency,
        //RespectForCustomers // Commented out one reason to see if this fixes layout issues
    }
    /*
    public enum AwardCategories
    {
        ChangeTheWorld=1,
        IntenselyFocusOnCustomers,
        MakeInnovationHappen,
        WinTogether,
        RespectAndCareForEachOther,
        AlwaysDoTheRightThing
    }*/

    public enum AwardAmounts
    {
        [Terms("25", "Twenty Five", "25 EUR"), Describe("25 EUR")] TwentyFive = 25,
        [Terms("50", "Fifty", "50 EUR"), Describe("50 EUR")] Fifty = 50,
        [Terms("100", "One Hundred", "100 EUR"), Describe("100 EUR")] OneHundred = 100,
        [Terms("200", "Two Hundred", "200 EUR"), Describe("200 EUR")] TwoHundred = 200,
        [Terms("500", "Five Hundred", "500 EUR"), Describe("500 EUR")] FiveHundred = 500
    }

    [Serializable]
    public class NominationForm
    {
        const string AWARD_MESSAGE_HELP =
            "Award messages should express gratitude for a colleauges committment to one or more of the pillars that your company ethod is built on. " +
            "Start by thanking them and then give some details of the situation where they helped you.";

        [Prompt("Great, can I have the name of the colleague you'd like to recognise please?")]
        [Describe("Nominee")]
        public string Name { get; set; }

        [Template(TemplateUsage.Feedback, Feedback = FeedbackOptions.Always)]
        [Prompt("Please choose the {&} you would like to award.\n {||}", ChoiceStyle = ChoiceStyleOptions.Buttons, ChoiceFormat = "{0}")]
        public AwardAmounts Amount { get; set; }

        [Prompt("What {&} will give this award?")]
        [Describe("Title")]
        public string Title { get; set; }

        [Template(TemplateUsage.Help, AWARD_MESSAGE_HELP)]
        [Prompt("What message would you like to send {Name} to thank them?")]
        public string Reason { get; set; }

        [Prompt("You can recognise a colleague for one of six award reasons, they are:\n {||}", ChoiceStyle = ChoiceStyleOptions.Buttons, ChoiceFormat = "{0}")]
        public AwardCategories Category { get; set; }

        [Prompt("Please choose the {&}\n {||}", ChoiceStyle = ChoiceStyleOptions.Buttons, ChoiceFormat = "{0}")]
        [Describe("Access level")]
        public AwardAccessOptions AwardAccess { get; set; }

        private Employee EmployeeChosenForNomination { get; set; }

        private string LastMessage { get; set; }

        public static IForm<NominationForm> BuildForm()
        {
            return new FormBuilder<NominationForm>()
                .Field(nameof(Name), validate: async (state, value) =>
                    {
                        var nameToCheck = (string) value;

                        var results = new ValidateResult {IsValid = true, Value = value};

                        var data = DataLookupHelper.Instance();
                        var employees = data.FindPotentialEmployees(nameToCheck);

                        if (employees.Count > 1)
                        {
                            string feedback =
                                "I found a few colleagues with similar names, did you mean any of the following?\n Type help to list available options.";

                            IEnumerable<Choice> choices = employees.Select((e, i) => new Choice
                            {
                                Description = new DescribeAttribute(e.ToString(), null, null, null, null),
                                Terms = new TermsAttribute {Alternatives = new[] {e.ToString(), (i + 1).ToString()}},
                                Value = e.ToString()
                            });

                            results.Choices = choices;
                            results.Feedback = feedback;
                            results.IsValid = false;
                        }
                        else if (employees.Count == 0)
                        {
                            results.Feedback = "You don\'t seem to work with anyone of that name.... ";
                            results.IsValid = false;
                        }
                        else
                        {
                            state.EmployeeChosenForNomination = employees[0];
                            results.Value = state.EmployeeChosenForNomination.ToString();
                        }

                        return results;
                    }
                ).Prompter(async (context, prompt, state, field) =>
                    {
                        var preamble = context.MakeMessage();
                        var promptMessage = context.MakeMessage();

                        if (prompt.GenerateMessages(preamble, promptMessage))
                        {
                            await context.PostAsync(preamble);
                        }

                        if (state.LastMessage !=
                            "I found a few colleagues with similar names, did you mean any of the following?\n Type help to list available options."
                        )
                        {
                            await context.PostAsync(promptMessage);
                        }

                        state.LastMessage = promptMessage.Text;

                        return prompt;
                    }
                )
                .Message("You selected: {Name}")
                .Field(nameof(Category))
                .Field(nameof(Amount))
                .Field(nameof(Title))
                .Field(nameof(Reason),
                    validate: async (state, value) =>
                    {
                        var message = (string) value;

                        var results = new ValidateResult {IsValid = true, Value = value};
                        var analyzer = new AwardAnalyzer();
                        var language = await analyzer.DetectLanguage(message);
                        var context = new Dictionary<string, object>();
                        if (language != null)
                        {
                            context.Add("language", language);
                        }

                        var analysis = await analyzer.AnalyzeAwardMessageAsync(message, context);
                        if (analysis.PositiviyScore < 0.35)
                        {
                            results.Feedback =
                                $@"You might like to know that your award message has a positivity score of {analysis.PositiviyScore:P2}, which is below the average positivity score.
                                                    Perhaps try using some positive words to describle your reasons for placing the award.";
                            results.IsValid = false;
                        }
                        else
                        {
                            results.Feedback =
                                $"Good Job! Your award message has a positivity score of {analysis.PositiviyScore:P2}";
                        }

                        return results;
                    })
                .AddRemainingFields()
                .Confirm("Does this look good to you?\n {*} {||}")
                .OnCompletion(Callback)
                .Build();
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(ToDto());
        }

        public NominationToPlace ToDto()
        {
            var data = DataLookupHelper.Instance();
            NominationToPlace n = new NominationToPlace
            {
                reason = data.GetAwardReasonId(Category.ToString()),
                title = Title,
                messageToRecipient = Reason,
                // messageToApprover = $"Please approve this award, {Title}",
                privacy = AwardAccess.ToString(),
                language = "eng"
            };

            n.nomineeAwards.Add(new NomineeAwards
                {nominee = EmployeeChosenForNomination.ID, awardType = data.GetAwardTypeId(Amount.ToString())});

            return n;
        }

        private static Task Callback(IDialogContext context, NominationForm state)
        {
            return context.PostAsync(
                $"Thanks {context.Activity.From.Name} for submitting this award. I'm sure {state.Name} will really appreciate it.");
        }
    }
}