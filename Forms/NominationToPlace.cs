﻿using System;
using System.Collections.Generic;

namespace SimpleEchoBot
{
    [Serializable]
    public class NomineeAwards
    {
        public string nominee { get; set; }
        public string awardType { get; set; }
    }

    [Serializable]
    public class NominationToPlace
    {
        public NominationToPlace()
        {
            nomineeAwards = new List<NomineeAwards>();
        }
        public string reason { get; set; }
        public string title { get; set; }
        public string messageToRecipient { get; set; }
       // public string messageToApprover { get; set; }
        public string privacy { get; set; }
        public string language { get; set; }

        public IList<NomineeAwards> nomineeAwards;      

    }
}