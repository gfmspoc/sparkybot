﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using SimpleEchoBot.BusinessEntities.Employee;
using SimpleEchoBot.Models;
using Newtonsoft.Json;
using Microsoft.Bot.Builder.FormFlow.Advanced;

namespace SimpleEchoBot.Forms
{
    public enum AwardAccessOptionsDL
    {
        [Describe("Privacy:Shared")]
        Shared = 1,
        [Describe("Privacy:Restricted")]
        Restricted,
        [Describe("Privacy:Private")]
        Private
    }


    public enum AwardCategoriesDL
    {
        [Describe("Category:Respect for All")]
        RespectForAll = 1,
        [Describe("Category:Innovation = Imagination + Determination")]
        Innovation,
        [Describe("Category:Respect for Quality")]
        RespectForQuality,
        [Describe("Category:Respect for Teamwork")]
        RespectForTeamwork,
        [Describe("Category:Respect for Urgency")]
        RespectForUrgency,
        [Describe("Category:Respect for Customers")]
        RespectForCustomers
    }
    /*
    public enum AwardCategoriesDL
    {
        [Describe("Category:Change The World")]
        ChangeTheWorld=1,
        [Describe("Category:Intensely Focus On Customers")]
        IntenselyFocusOnCustomers,
        [Describe("Category:Make Innovation Happen")]
        MakeInnovationHappen,
        [Describe("Category:Win Together")]
        WinTogeather,
        [Describe("Category:Respect And Care For Each Other")]
        RespectAndCareForEachOther,
        [Describe("Category:Always Do The Right Thing")]
        AlwaysDoTheRightThing
    }
    */
    public enum AwardAmountsDL
    {
        [Terms(new[] { "25", "Twenty Five" })]
        [Describe("Amount:Twenty Five")]
        TwentyFive = 25,
        [Terms(new[] { "50", "Fifty" })]
        [Describe("Amount:Fifty")]
        Fifty = 50,
        [Terms(new []{"100", "One Hundred"})]
        [Describe("Amount:One Hundred")]
        OneHundred = 100,
        [Terms(new[] { "200", "Two Hundred" })]
        [Describe("Amount:Two Hundred")]
        TwoHundred = 200,
        [Terms(new[] { "500", "Five Hundred" })]
        [Describe("Amount:Five Hundred")]
        FiveHundred = 500
    }

    [Serializable]
    public class NominationFormDirectline
    {
       const string AWARD_MESSAGE_HELP = "Award messages should express gratitude for a colleauges committment to one or more of the pillars that your company ethod is built on. " +
      "Start by thanking them and then give some details of the situation where they helped you.";

        [Prompt("Great, can I have the name of the colleague you'd like to recognise please?")]
        [Describe("Nominee")]
        public string Name { get; set; }

        [Template(TemplateUsage.Feedback, Feedback = FeedbackOptions.Always)]
        [Prompt("Please choose the {&} you would like to award. {||}")]
        public AwardAmountsDL Amount { get; set; }

        [Prompt("What {&} will give this award?")]
        [Describe("Title")]
        public string Title { get; set; }

        [Template(TemplateUsage.Help, new string[] { AWARD_MESSAGE_HELP })]
        [Prompt("What message would you like to send {Name} to thank them?")]
        public string Reason { get; set; }
      
        [Prompt("You can recognise a colleague for one of six award reasons, they are: {||}")]
        public AwardCategoriesDL Category { get; set; }

        [Template(TemplateUsage.StringHelp)]
        [Prompt("Please choose the {&} {||}")]
        [Describe("Access level")]
        public AwardAccessOptionsDL AwardAccess { get; set; }

        private Employee employeeChosenForNomination { get; set; }

        private string LastMessage { get; set; }

        static IList<AwardReason> AwardReasonClassifications;

        public static IForm<NominationFormDirectline> BuildForm()
        {
            return new FormBuilder<NominationFormDirectline>()
                .Field(nameof(Name), validate: async (state, value) =>
                            {
                                var data = DataLookupHelper.Instance();

                                var results = new ValidateResult { IsValid = true, Value = value };
                                var nameToCheck = (string)value;
                                var employees = data.FindPotentialEmployees(nameToCheck);

                                if (employees.Count > 1)
                                {
                                    string feedback = "I found a few colleagues with similar names, did you mean any of the following?";

                                    IEnumerable<Choice> choices = employees.Select(e => new Choice()
                                    {
                                        Description = new DescribeAttribute($"Name:{e.ToString()}", null, null, null, null),
                                        Terms = new TermsAttribute() { Alternatives = new[] { e.ToString() } },
                                        Value = e.ToString()
                                    });

                                    results.Choices = choices;
                                    results.Feedback = feedback;
                                    results.IsValid = false;
                                }
                                else if (employees.Count == 0)
                                {
                                    results.Feedback = $"You don't seem to work with anyone of that name.... ";
                                    results.IsValid = false;
                                }
                                else
                                {
                                    state.employeeChosenForNomination = employees[0];
                                    results.Value = state.employeeChosenForNomination.ToString();
                                }
                                
                                return results;
                            }
                            )
                            .Prompter(async (context, prompt, state, field) =>
                            {
                                var preamble = context.MakeMessage();
                                var promptMessage = context.MakeMessage();

                                if (prompt.GenerateMessages(preamble, promptMessage))
                                {
                                    await context.PostAsync(preamble);
                                }

                                if (state.LastMessage != "I found a few colleagues with similar names, did you mean any of the following?")
                                {
                                    await context.PostAsync(promptMessage);
                                }

                                state.LastMessage = promptMessage.Text;

                                return prompt;
                            }
                            )
                            .Message("You selected: {Name}")
                            
                .Field(nameof(Category))
                .Field(nameof(Amount))
                .Field(nameof(Title))
                        .Field(nameof(Reason),
                            validate: async (state, value) =>
                            {
                                var results = new ValidateResult { IsValid = true, Value = value };
                                var analyzer = new AwardAnalyzer();
                                var message = (string)value;
                                var language = await analyzer.DetectLanguage(message);
                                var context = new Dictionary<string, object>();
                                if (language != null)
                                {
                                    context.Add("language", language);
                                }
                                var analysis = await analyzer.AnalyzeAwardMessageAsync(message, context);
                                if (analysis.PositiviyScore < 0.35)
                                {
                                    results.Feedback = $@"You might like to know that your award message has a positivity score of {analysis.PositiviyScore:P2}, which is below the average positivity score.
                                                            Perhaps try using some positive words to describle your reasons for placing the award.";
                                    results.IsValid = false;
                                }
                                else
                                {
                                    AwardReasonClassifications = analysis.Reasons;
                                    results.Feedback = $"Good Job! Your award message has a positivity score of {analysis.PositiviyScore:P2}";
                                }
                                
                                return results;
                            })
                        .AddRemainingFields()
                        .Confirm("Does this look good to you?\n {*} {||}")
                        .OnCompletion(Callback)
                        .Build();

        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(ToDto());
        }

        public NominationToPlace ToDto()
        {
            var data = DataLookupHelper.Instance();
            NominationToPlace n = new NominationToPlace()
            {
                reason = data.GetAwardReasonId(Category.ToString()),
                title = Title,
                messageToRecipient = Reason,
               // messageToApprover = $"Please approve this award, {Title}",
                privacy = AwardAccess.ToString(),
                language = "eng"
                
            };

            n.nomineeAwards.Add(new NomineeAwards { nominee = employeeChosenForNomination.ID, awardType = data.GetAwardTypeId( Amount.ToString()) });

            return n;
        }
        private static Task Callback(IDialogContext context, NominationFormDirectline state)
        {
            return context.PostAsync($"Thanks {context.Activity.From.Name} for submitting this award. I'm sure {state.Name} will really appreciate it.");
        }
    }
}
