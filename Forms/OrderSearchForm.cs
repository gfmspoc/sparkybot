﻿using Microsoft.Bot.Builder.FormFlow;
using System;
using Newtonsoft.Json;
using SimpleEchoBot.Exceptions;

namespace SimpleEchoBot.Forms
{
    public enum OrderTypes
    {
        GiftCard = 1,
        ECert,
        Merchandise
    }

    [Serializable]
    public class OrderSearchForm
    {
      //  [Prompt("When did you place this order?")]
      //  [Describe("OrderDate")]
     //   public DateTime OrderDate { get; set; }

      //  [Prompt("What type of order was it?  {||}")]
      //  [Describe("OrderType")]
      //  public OrderTypes OrderType { get; set; }      

        public static IForm<OrderSearchForm> BuildForm()
        {

            
            return new FormBuilder<OrderSearchForm>()
                
              /*  .Field(nameof(OrderType), validate: async (state, value) =>
                {
                    var results = new ValidateResult { IsValid = true, Value = value };
                    string feedback = "";
                    
                    OrderTypes ot = (OrderTypes)Enum.Parse(typeof(OrderTypes), value.ToString());
                    
                    if (ot == OrderTypes.ECert)
                    {

                           results.IsValid = true;
                           results.Feedback = feedback;
                        
                     
                    }
                    return results;
                }
                )
                .Confirm("So I am looking for an order that matches the following - right?\n {*} {||}")    */      
                .Build();
                
        }
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
