﻿using System;
using System.Collections.Generic;

namespace SimpleEchoBot.Forms
{
    public class ProductList
    {
        static ProductList _instance = null;

        public static ProductList Instance()
        {
            if (_instance == null) PopulateList();

            return _instance;
        }

        public List<Product> GetList()
        {
            return _list;
        }

        List<Product> _list;
        private static void PopulateList()
        {
            _instance = new ProductList()
            {
                _list = new List<Product>()
            };
            _instance._list.Add(new Product { Id = "WWWAMURFCG", Title = "Amazon.co.uk eGift Card", SalesType = Product.SalesType_Ecert, Denominations = { 10, 25, 50, 100 }, Fx = "GBP", Discount = 0.02m });
            _instance._list.Add(new Product { Id = "IRLCOMCURR", Title = "Currys", SalesType = Product.SalesType_GiftCard, Denominations = { 10, 50 }, Fx = "EUR", Discount = 0.06m });
            _instance._list.Add(new Product { Id = "IRLHOMHARV", Title = "Harvey Norman", SalesType = Product.SalesType_GiftCard, Denominations = { 50 }, Fx = "EUR", Discount = 0.1m });
            _instance._list.Add(new Product { Id = "URLCIRRFCG", Title = "Clarks - eGift Card", SalesType = Product.SalesType_Ecert, Denominations = { 25 }, Fx = "EUR", Discount = 0.06m });

        }
    }

    [Serializable]
    public class Product
    {
        public static readonly string SalesType_Ecert = "eCerts";
        public static readonly string SalesType_GiftCard = "Gift Cards";
        public Product()
        {
            Denominations = new List<int>();
        }

        public string Id { get; set; }
        public string Title { get; set; }
        public decimal Discount { get; set; }
        public string Fx { get; set; }
        public string SalesType { get; set; }
        public List<int> Denominations { get; set; }

        public decimal Price(int denmo)
        {
            var discount = denmo * Discount;
            return denmo - discount;
        }
    }
}