﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;


namespace SimpleEchoBot.BusinessEntities.Employee
{
    [Serializable]
    [XmlRoot("EmployeeList")]
    public class EmployeeList
    {
        public EmployeeList() { Items = new List<Employee>(); }
        [XmlElement("Employee")]
        public List<Employee> Items { get; set; }
    }

    [Serializable]
    public class Employee
    {

        [XmlElement("ID")]
        public string ID { get; set; }

        [XmlElement("FirstName")]
        public string firstName { get; set; }

        [XmlElement("LastName")]
        public string lastName { get; set; }

        [XmlElement("department")]
        public string department { get; set; }


        [XmlElement("ManagerName")]
        public string managerName { get; set; }


        public override string ToString()
        {
            return $"{firstName} {lastName}";
        }
    }
}
