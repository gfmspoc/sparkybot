﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.FormFlow.Advanced;

namespace SimpleEchoBot.Forms
{
    public enum DemoAddresses
    {
        [Describe("Globoforce Ltd, 19 Beckett Way")]
        [Terms("Globoforce", "Globoforce Ltd, 19 Beckett Way")]
        Globoforce = 1,
        [Describe("123 Main Street, Clontarf")]
        [Terms("123", "123 Main Street, Clontarf")]
        MainStreet,
        [Describe("456 Lower River Street, Kildare")]
        [Terms("456", "456 Lower River Street, Kildare")]
        LowerStreet
    };

    public enum DemoDenominations
    {
        [Describe("10")]
        [Terms("10", "Ten")]
        Ten = 1,
        [Describe("25")]
        [Terms("25", "Twenty Five")]
        TwentyFive,
        [Describe("50")]
        [Terms("50", "Fifty")]
        Fifty,
        [Describe("100")]
        [Terms("100", "One Hundred")]
        OneHundred
    }
    [Serializable]
    public class RedemptionForm
    {
        [Prompt("What would you like to order? {||}")]
        [Describe("Product")]
        public string ItemId { get; set; }
        private Product _selectedProduct { get; set; }

        [Prompt("Please select the value you would like {||}", ChoiceStyle = ChoiceStyleOptions.Buttons)]
        [Template(TemplateUsage.NoPreference, "None")]
        public DemoDenominations Denomination { get; set; }

        [Prompt("How many would you like?")]
        public int Quantity { get; set; }

        [Prompt("Which {&} will we use? {||}")]
        public DemoAddresses Address { get; set; }

        [Prompt("Please enter your {&}")]
        [Optional]
        public string EmailAddress { get; set; }

        [Prompt("Do you accept Globoforce's {&}")]
        [Describe("Terms and Conditions")]
        public bool AcceptTerms { get; set; }

        public static IForm<RedemptionForm> BuildForm()
        {
            return new FormBuilder<RedemptionForm>()
                .Field(new FieldReflector<RedemptionForm>(nameof(ItemId))
                .SetType(null)
                .SetDefine((state, field) =>
                {
                    var products = ProductList.Instance().GetList();
                    int i = 1;
                    foreach(var p in products)
                    {
                        field.AddDescription(p.Id, p.Title)
                        .AddTerms(p.Id, p.Title, i.ToString());
                        i++;
                    }

                    return Task.FromResult(true);
                })
                .SetValidate((state, value) =>
                {
                    var productToCheck = (string)value;

                    var results = new ValidateResult { IsValid = true, Value = value };

                    var products = ProductList.Instance().GetList();
                    var selectedProduct = products.FirstOrDefault(p => p.Id.Equals(productToCheck, StringComparison.OrdinalIgnoreCase));

                    if (selectedProduct == null)
                    {
                        string feedback = "I didn't find anything like that, select one of the following";

                        IEnumerable<Choice> choices = products.Select((p, i) => new Choice()
                        {
                            Description = new DescribeAttribute(p.Title, null, null, null, null),
                            Terms = new TermsAttribute() { Alternatives = new[] { p.Title, (i + 1).ToString() } },
                            Value = p.Id
                        });

                        results.Choices = choices;
                        results.Feedback = feedback;
                        results.IsValid = false;
                    }
                    else
                    {
                        state.ItemId = selectedProduct.Id;
                        results.Value = selectedProduct.Id;
                        state._selectedProduct = selectedProduct;
                    }

                    return Task.FromResult(results);
                })
                ).Field(new FieldReflector<RedemptionForm>(nameof(Denomination))
                .SetActive((state) => state._selectedProduct != null))
                .Field(nameof(Quantity))
                .Field(new FieldReflector<RedemptionForm>(nameof(Address))
                .SetActive((state) => state._selectedProduct != null && state._selectedProduct.SalesType == Product.SalesType_GiftCard)
                ).Field(new FieldReflector<RedemptionForm>(nameof(EmailAddress))
                .SetDependencies(new[] { nameof(ItemId) })
                .SetActive((state) => state._selectedProduct != null && state._selectedProduct.SalesType == Product.SalesType_Ecert))
                .AddRemainingFields()
                .Confirm("Does this look good to you?\n {*} {||}")
                .OnCompletion(Callback)
                .Build();
        }

        private static Task Callback(IDialogContext context, RedemptionForm state)
        {
            return context.PostAsync($"Thanks {context.Activity.From.Name} for placing the order. You should receive a confirmation of this order shortly");
        }
    }
}