﻿using SimpleEchoBot.BusinessEntities.Employee;
using SimpleEchoBot.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SimpleEchoBot.Forms
{
    public class DataLookupHelper
    {
        static DataLookupHelper _instance = null;

        public static DataLookupHelper Instance()
        {
            if (_instance == null) _instance = new DataLookupHelper();

            return _instance;
        }

        private IList<Employee> _employees;

        private List<AwardReasonTable> _art;

        private List<AwardTypeTable> _att;

        private IList<GiftCard> _gct;

        public string GetAwardReasonId(string awardReasonName)
        {
            loadAwardReasonTable();
            return _art.FirstOrDefault(a => a.enumVal.Equals(awardReasonName, StringComparison.OrdinalIgnoreCase))?.pkid;
        }

        public string GetAwardTypeId(string awardValue)
        {
            loadAwardTypeTable();
            return _att.FirstOrDefault(a => a.enumValue.Equals(awardValue, StringComparison.OrdinalIgnoreCase))?.pkAwardType;
        }
        public IList<Employee> FindPotentialEmployees(string nameToCheck)
        {
            loadSomeEmployees();

            if (string.IsNullOrWhiteSpace(nameToCheck)) return new List<Employee>();

            var names = nameToCheck.Split(' ');

            string firstName = names.Length > 0 ? names[0] : string.Empty;
            string lastName = names.Length > 1 ? names[1] : string.Empty;

            return _employees.Where(e => e.firstName.StartsWith(firstName, StringComparison.OrdinalIgnoreCase) &&
                                        (lastName.Equals(string.Empty) || e.lastName.Equals(lastName, StringComparison.OrdinalIgnoreCase)))
                .ToList();

        }

        public IList<GiftCard> FindGiftCards(string nameToCheck)
        {
            loadSomeGiftcards();

            if (string.IsNullOrWhiteSpace(nameToCheck)) return new List<GiftCard>();

            string firstName = nameToCheck.Length > 0 ? nameToCheck : string.Empty;

            return _gct.Where(e => e.description.Contains(firstName))
                .ToList();
        }

        private void loadAwardReasonTable()
        {
            if (_art != null) return;

            string AppPath = HttpContext.Current.Server.MapPath(".");
            string xmlPath = AppPath + @"\BusinessEntities\XML\AwardReasonTable.xml";

            using (var reader = new StreamReader(xmlPath))
            {
                XmlSerializer deserializer = new XmlSerializer(typeof(List<AwardReasonTable>),
                new XmlRootAttribute("AwardReasonList"));
                _art = (List<AwardReasonTable>)deserializer.Deserialize(reader);
            }
        }

        private void loadAwardTypeTable()
        {
            if (_att != null) return;

            string AppPath = HttpContext.Current.Server.MapPath(".");
            string xmlPath = AppPath + @"\BusinessEntities\XML\AwardTypeTable.xml";

            using (var reader = new StreamReader(xmlPath))
            {
                XmlSerializer deserializer = new XmlSerializer(typeof(List<AwardTypeTable>),
                new XmlRootAttribute("AwardTypeList"));
                _att = (List<AwardTypeTable>)deserializer.Deserialize(reader);
            }
        }

        private void loadSomeEmployees()
        {
            if (_employees != null) return;

            string AppPath = HttpContext.Current.Server.MapPath(".");
            string xmlPath = AppPath + @"\BusinessEntities\XML\Employees.xml";

            using (var reader = new StreamReader(xmlPath))
            {
                XmlSerializer deserializer = new XmlSerializer(typeof(List<Employee>),
                    new XmlRootAttribute("EmployeeList"));
               
                _employees = (List<Employee>)deserializer.Deserialize(reader);
            }
        }

        private void loadSomeGiftcards()
        {
            if (_gct != null) return;

            string AppPath = HttpContext.Current.Server.MapPath(".");
            string xmlPath = AppPath + @"\BusinessEntities\XML\VouchersTable.xml";

            using (var reader = new StreamReader(xmlPath))
            {
                XmlSerializer deserializer = new XmlSerializer(typeof(List<GiftCard>),
                    new XmlRootAttribute("GiftCardList"));

                _gct = (List<GiftCard>)deserializer.Deserialize(reader);
            }
        }
    }
}