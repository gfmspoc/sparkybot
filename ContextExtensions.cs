﻿using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;

namespace SimpleEchoBot.Extras
{
    public static class ContextExtensions
    {
        public static async Task SendEventToBot(this IDialogContext context, string name, string value)
        {
            var activity = context.MakeMessage() as IEventActivity;
            activity.Type = "event";
            activity.Name = name;
            activity.Value = value;

            await context.PostAsync((IMessageActivity) activity);
        }
    }
}